package app.com.jumedicine.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import app.com.jumedicine.Bean.SearchBean;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 12-Dec-17.
 */

public class SearchAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<SearchBean> arrayList;

    public SearchAdapter(Context context, ArrayList<SearchBean> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.file_row2, null);


        TextView catName =  convertView.findViewById(R.id.categoryTxt);
        TextView fileName =  convertView.findViewById(R.id.fileTXT);
        ImageView img = convertView.findViewById(R.id.tvTitle_row);
        catName.setText(arrayList.get(position).getPostName());
        fileName.setText(arrayList.get(position).getPost_type());

        Glide.with(context).load(arrayList.get(position).getIcon()).into(img);
      //  GlideApp.with(this).load("http://goo.gl/gEgYUd").into(imageView);
        //title.setText((position+1)+"");



        return convertView;
    }
}