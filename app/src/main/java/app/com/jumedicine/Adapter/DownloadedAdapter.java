package app.com.jumedicine.Adapter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import app.com.jumedicine.Activity.CourseListCategory2;
import app.com.jumedicine.Activity.DialogRename;
import app.com.jumedicine.Activity.DownloadedFileActivity;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 09-Dec-17.
 */

public class DownloadedAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CourseFile> arrayList;
    Activity activity;


    public DownloadedAdapter (Context context, ArrayList<CourseFile> arrayList,Activity activity) {
        this.context = context;
        this.arrayList = arrayList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.file_row, null);

        ImageView deleteBtn = convertView.findViewById(R.id.deleteBtn);
        ImageView editBtn = convertView.findViewById(R.id.editBtn);
        deleteBtn.setVisibility(View.VISIBLE);
        editBtn.setVisibility(View.VISIBLE);
        TextView title = convertView.findViewById(R.id.tvTitle_row);
        TextView catName =  convertView.findViewById(R.id.categoryTxt);
        TextView dateTxt = convertView.findViewById(R.id.dateTxt);
        dateTxt.setVisibility(View.VISIBLE);
        catName.setVisibility(View.GONE);
        TextView fileName =  convertView.findViewById(R.id.fileTXT);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog alertDialog = new AlertDialog.Builder(context,R.style.MyDialogTheme).create();
                alertDialog.setTitle("Delete File");
                alertDialog.setMessage("Are you sure to delete this file ?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        try {
                            File file2 = new File(arrayList.get(position).getFile());
                            File file = new File(file2.getPath());
                            file.delete();
                            if (file.exists()) {
                                file.getCanonicalFile().delete();
                                if (file.exists()) {
                                    context.deleteFile(file.getName());
                                }

                            }
                            arrayList.remove(position);
                            notifyDataSetChanged();
                            Log.e("newSize",arrayList.size()+"");
                        }catch (Exception e){
                            Log.e("EXCEPTION",e+"");
                        }

                    } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        alertDialog.dismiss();
                    }});

                alertDialog.show();
            }
        });
        dateTxt.setText(arrayList.get(position).getDate());
        fileName.setText(arrayList.get(position).getName());
        title.setText((position+1)+"");

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = ((FragmentActivity)activity).getSupportFragmentManager()
                        .beginTransaction();
                    DialogRename editNameDialogFragment = DialogRename.newInstance("Some Title",
                            "/sdcard/JU_Medicine/",
                            arrayList.get(position).getName());
                    editNameDialogFragment.show(ft, "fragment_edit_name");
            }
        });
        return convertView;
    }


}