package app.com.jumedicine.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.jumedicine.Bean.FeedbackBean;
import app.com.jumedicine.Bean.ReadBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;
import app.com.jumedicine.Utill.VolleyRequest;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedBack extends Fragment {

    private FeedBack.OnFragmentInteractionListener fragmentInteractionListener;

    static EditText name , email,message,subject;
    Button send;
    Preferances preferances;
    static String name_ , email_,message_,subject_;
    static ProgressDialog pd ;
    RelativeLayout messageRl;
    VolleyRequest req;
    static String url ;
    static Context context;

    public FeedBack() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_back,container,false);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait...");
        preferances = new Preferances(getActivity());

        url = preferances.getSiteURL()+Constance.MAILER;
        context = getActivity();
        messageRl = view.findViewById(R.id.messageRl);
        name = view.findViewById(R.id.nameEdt);
        email = view.findViewById(R.id.emailEdt);
        message = view.findViewById(R.id.messageEdt);
        subject = view.findViewById(R.id.subjectEdt);

        messageRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message.requestFocus();
            }
        });
        send = view.findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("")) {
                    Toast.makeText(getActivity(),"Please Fill The Name",Toast.LENGTH_LONG).show();
                    name.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(name, InputMethodManager.SHOW_IMPLICIT);
                }else if (email.getText().toString().equals("")){
                    Toast.makeText(getActivity(),"Please Fill The Email",Toast.LENGTH_LONG).show();
                    email.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(email, InputMethodManager.SHOW_IMPLICIT);
                }else if (message.getText().equals("")){
                    Toast.makeText(getActivity(),"Please Fill The Message",Toast.LENGTH_LONG).show();
                    message.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(message, InputMethodManager.SHOW_IMPLICIT);
                }else if (subject.getText().toString().equals("")){
                    Toast.makeText(getActivity(),"Please Fill The Subject",Toast.LENGTH_LONG).show();
                    subject.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(subject, InputMethodManager.SHOW_IMPLICIT);
                }
                else{
                    name_ = name.getText().toString();
                    email_ = email.getText().toString();
                    message_ = message.getText().toString();
                    subject_ = subject.getText().toString();

                    FeedbackBean bean = new FeedbackBean();
                    bean.setEmail(email_);
                    bean.setMassage(message_);
                    bean.setName(name_);
                    bean.setType("contact_us");
                    bean.setSubject(subject_);
                    Gson gson = new Gson();
                    String json = gson.toJson(bean);

                    pd.show();
                    //Request(json);
                    DownloadInfoOfWeather d = new DownloadInfoOfWeather();
                    d.execute();
                }
            }
        });

        return view;
    }
    private void Request(String json)
    {

        req = new VolleyRequest(Request.Method.POST, preferances.getSiteURL() + Constance.MAILER, null, json, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.v("Response:%n %s", response);
                try {
                    if (response.equals("sucess")){
                        name.setText("");
                        email.setText("");
                        message.setText("");
                        subject.setText("");
                        Toast.makeText(getActivity(),"successfully send",Toast.LENGTH_LONG).show();
                    }
                    pd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                error.printStackTrace();
                pd.dismiss();

            }
        });
        Volley.newRequestQueue(getActivity()).add(req);
     /*   RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,preferances.getSiteURL()+ Constance.MAILER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Response",response);
                if (response.equals("sucess")){
                    name.setText("");
                    email.setText("");
                    message.setText("");
                    subject.setText("");

                }
                pd.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                params.put("name",name_);
                params.put("email",email_);
                params.put("massage",message_);
                params.put("type","contact_us");
                params.put("subject",subject_);

                Log.e("JSON",params+"");
                return params;
            }                Map<String, String>  params = new HashMap<String, String>();

        };

        queue.add(stringRequest);*/
    }

    public class OnFragmentInteractionListener {
    }

    public static class DownloadInfoOfWeather extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name", name_));
                nameValuePairs.add(new BasicNameValuePair("email", email_));
                nameValuePairs.add(new BasicNameValuePair("massage", message_));
                nameValuePairs.add(new BasicNameValuePair("Subject",subject_));
                nameValuePairs.add(new BasicNameValuePair("type", "contact_us"));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                //  Log.e("res",response.get)
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            name.setText("");
            email.setText("");
            message.setText("");
            subject.setText("");
            Toast.makeText(context,"successfully send",Toast.LENGTH_LONG).show();


        }
    }

}
