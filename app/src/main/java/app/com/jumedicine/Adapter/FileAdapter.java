package app.com.jumedicine.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 09-Dec-17.
 */

public class FileAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CourseFile> arrayList;

    public FileAdapter(Context context, ArrayList<CourseFile> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.file_row, null);


        TextView title = convertView.findViewById(R.id.tvTitle_row);
        TextView catName =  convertView.findViewById(R.id.categoryTxt);
        TextView fileName =  convertView.findViewById(R.id.fileTXT);
        catName.setText(arrayList.get(position).getGroup_name());
        fileName.setText(arrayList.get(position).getName());
        title.setText((position+1)+"");



        return convertView;
    }
}