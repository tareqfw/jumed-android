package app.com.jumedicine.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.CourseListCatAdapter;
import app.com.jumedicine.Adapter.FileAdapter;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.Bean.CourseListCatBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class CourseListCategory extends AppCompatActivity {

    ArrayList<CourseListCatBean> courseArrayList;
    ArrayList<CourseFile> fileArrayList;
    String url;
    CourseListCatBean bean;
    ListView listView ,fileListView;
    static int postID = 0;
    Boolean check = false;
    ProgressDialog pd;
    String catURL ,download_file_url;
    RelativeLayout postTv_Rl , fileTv_Rl;
    String dest_file_path = "test.pdf";
    int downloadedSize = 0, totalsize;
    float per = 0;
    public static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE= 1;
    String file,fName;
    ProgressDialog progressDialog;
    TextView titleTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list_category);

        pd = new ProgressDialog(CourseListCategory.this);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);

        progressDialog = new ProgressDialog(CourseListCategory.this);
        progressDialog.setMessage("Downloading...");
        progressDialog.setCancelable(false);

        postTv_Rl = findViewById(R.id.postTV_Rl);
        fileTv_Rl = findViewById(R.id.fileTV_Rl);


        ImageView notiBtn = findViewById(R.id.notiBtn);
        notiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });

        titleTv = findViewById(R.id.titleTTV);
        try {
            titleTv.setText(getIntent().getExtras().getString("name"));
        }catch (Exception e){
            Log.e("ERROR",e+"");
        }
        Preferances preferances = new Preferances(getApplicationContext());
        catURL = preferances.getSiteURL()+ Constance.GET_CATEGORY;
        courseArrayList = new ArrayList<>();
        fileArrayList = new ArrayList<>();

        courseArrayList = (ArrayList<CourseListCatBean>) getIntent().getSerializableExtra("arrayListCat");
        fileArrayList = (ArrayList<CourseFile>) getIntent().getSerializableExtra("arrayListFile");

        Log.e("Size 1",courseArrayList.size()+"");
        Log.e("Size 2",fileArrayList.size()+"");
       // Toast.makeText(getApplicationContext(),fileArrayList.size()+"",Toast.LENGTH_LONG).show();


        listView = findViewById(R.id.list_view_cat);

        fileListView = findViewById(R.id.list_view_cat_file);

        if (fileArrayList.size()>0){
            FileAdapter fileAdapter = new FileAdapter(getApplicationContext(),fileArrayList);
            fileListView.setAdapter(fileAdapter);


        }
        else{
            fileTv_Rl.setVisibility(View.GONE);
            fileListView.setVisibility(View.GONE);
        }

        fileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                file = "";
                file = fileArrayList.get(position).getFile();
                final AlertDialog alertDialog = new AlertDialog.Builder(CourseListCategory.this,R.style.MyDialogTheme).create();
                alertDialog.setTitle(fileArrayList.get(position).getName());
                alertDialog.setMessage("What to do with this file ?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Open", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fileArrayList.get(position).getFile()));
                        startActivity(browserIntent);

                    } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Download", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            Log.e("if ", "if 0 ");

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(CourseListCategory.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                                // Toast.makeText(getApplicationContext(),"IF 1 ",Toast.LENGTH_LONG).show();

                                ActivityCompat.requestPermissions(CourseListCategory.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","if");
                                // Show an explanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(CourseListCategory.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","else");
                                // Toast.makeText(getApplicationContext(),"IF 2 ",Toast.LENGTH_LONG).show();
                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }else{
                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root,getResources().getString(R.string.folderName));
                            if (!myDir.exists())
                                myDir.mkdirs();
                            String mUrl=file;
                            //RequestQueue mRequestQueue;
                            fName = fileArrayList.get(position).getName()+".pdf";
                            progressDialog.show();
                            AsyncTask asyncTask = new AsyncTask();
                            asyncTask.execute();

                        }




                    }});

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        alertDialog.dismiss();

                    }});
                alertDialog.show();
            }
        });

        CourseListCatAdapter adapter = new CourseListCatAdapter(getApplicationContext(),courseArrayList);
        listView.setAdapter(adapter);
//        postID = Integer.parseInt(getIntent().getExtras().getString("postID"));
   //     Log.e("postID",postID+"");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(),arrayList.get(position).getPostName().toString(),Toast.LENGTH_LONG).show();
                try {
                    postID = courseArrayList.get(position).getPostID();
                    pd.show();
                }catch (Exception e){
                    Log.e("ERROR",e+"");
                }

                StringRequest postRequest = new StringRequest(Request.Method.POST, catURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("RESPONSEDDDDD",response );
                                try {
                                    pd.dismiss();
                                    JSONObject jsonResponse = new JSONObject(response);

                                    String type = jsonResponse.getString("type");
                                    if (type.equals("category")){
                                       /* Intent intent = new Intent(getApplicationContext(),CourseListCategory.class);
                                        intent.putExtra("arrayListCat",courseArrayList);
                                        intent.putExtra("arrayListFile",fileArrayList);
                                        startActivity(intent);*/

                                    JSONArray jsonPosts ,jsonFile;
                                    jsonPosts = jsonResponse.getJSONArray("posts");
                                    Log.e("obj",jsonPosts+"");
                                    //JSONArray postArray = new JSONArray(jsonResponse.getJSONArray("posts"));
                                    courseArrayList = new ArrayList<>();
                                    fileArrayList = new ArrayList<>();
                                    for (int i=0; i<jsonPosts.length() ; i++){
                                        CourseListCatBean bean = new CourseListCatBean();
                                        bean.setPostID(jsonPosts.getJSONObject(i).getInt("postID"));
                                        bean.setPostName(jsonPosts.getJSONObject(i).getString("postName"));
                                        bean.setIcon(jsonPosts.getJSONObject(i).getString("icon"));
                                        Log.e("PostName",bean.getPostName()+"");
                                        courseArrayList.add(bean);
                                    }

                                    Log.e("SizeFile",courseArrayList.size()+"");
                                    jsonFile = jsonResponse.getJSONArray("file");
                                    for (int i=0; i<jsonFile.length() ; i++){
                                        CourseFile bean = new CourseFile();
                                        bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                        bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                        bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                        Log.e("PostName",bean.getName()+"");
                                        fileArrayList.add(bean);
                                    }
                                    Log.e("SizeFile",fileArrayList.size()+"");

                                    Log.e("TYPE",jsonResponse.getString("type")+"");

                                        Intent intent = new Intent(getApplicationContext(),CourseListCategory.class);
                                        intent.putExtra("arrayListCat",courseArrayList);
                                        intent.putExtra("arrayListFile",fileArrayList);
                                        startActivity(intent);


                                    }else{
                                        JSONArray jsonFile;
                                        String title , content;
                                        jsonFile = jsonResponse.getJSONArray("file");

                                        title = jsonResponse.getString("title");
                                        content = jsonResponse.getString("content");
                                    fileArrayList = new ArrayList<>();
                                        for (int i=0; i<jsonFile.length() ; i++){
                                            CourseFile bean = new CourseFile();
                                            bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                            bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                            bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                            Log.e("PostName",bean.getName()+"");
                                            fileArrayList.add(bean);
                                        }

                                        Log.e("title",title);
                                        Log.e("content",content);
                                        Log.e("SizeFile",fileArrayList.size()+"");

                                        Intent intent = new Intent(getApplicationContext(),FileListActivity.class);
                                        intent.putExtra("title",title);
                                        intent.putExtra("drName",content);
                                        intent.putExtra("arrayListFile",fileArrayList);
                                        startActivity(intent);

                                    }

                                } catch (JSONException e) {
                                    //e.printStackTrace();
                                    Log.e("ERROR",e+"");
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                pd.dismiss();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("cat_id", postID+"");
                        return params;
                    }
                };
                Volley.newRequestQueue(getApplicationContext()).add(postRequest);
            }
        });
        //Request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                   // Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root,getResources().getString(R.string.folderName));
                    if (!myDir.exists())
                        myDir.mkdirs();
                    else
                        Log.e("Folder","exist");


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public class AsyncTask extends android.os.AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL u = new URL(file);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                FileOutputStream f = new FileOutputStream(new File("/sdcard/"+getResources().getString(R.string.folderName), fName));


                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ( (len1 = in.read(buffer)) > 0 ) {
                    f.write(buffer,0, len1);
                }
                f.close();
            }catch (Exception e){
                Log.e("ERROR",e+"");
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(),"Downloaded",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
