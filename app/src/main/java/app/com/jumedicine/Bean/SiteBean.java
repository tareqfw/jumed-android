package app.com.jumedicine.Bean;


/**
 * Created by abu_a on 23-Nov-17.
 */

public class SiteBean {

    private String siteUrl,SiteName;

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getSiteName() {
        return SiteName;
    }

    public void setSiteName(String siteName) {
        SiteName = siteName;
    }

    @Override
    public String toString() {
        return SiteName ;
    }
}
