package app.com.jumedicine.Activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.YearsAdapter;
import app.com.jumedicine.Bean.CourseFile2;
import app.com.jumedicine.Bean.CourseListBean;
import app.com.jumedicine.Bean.CourseListCatBean2;
import app.com.jumedicine.Bean.GetMenuBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

/**
 * A simple {@link Fragment} subclass.
 */
public class YearsFragment extends Fragment {

    private YearsFragment.OnFragmentInteractionListener fragmentInteractionListener;
    String url,catURL,name;
    ArrayList<GetMenuBean> arrayList;
    CourseListBean bean;
    ListView listView ;
    static int postID = 0;
    Boolean check = false;
    ProgressDialog pd ;
    ArrayList<GetMenuBean> arrayListCat;
    //ArrayList<CourseFile2> arrayListFile;
    Preferances preferances;

    public YearsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.e("Size", arrayListCat.size() + "");
            Log.e("Size", arrayListCat.size() + "");
        }catch (Exception e){

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_years,container,false);
    pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);
        this.pd.show();
        preferances = new Preferances(getActivity());
        /*arrayListCat = new ArrayList<>();
        arrayListFile = new ArrayList<>();*/
        arrayList = new ArrayList<>();
        arrayListCat = new ArrayList<>();
        url = preferances.getSiteURL()+Constance.GET_MENU;

        listView = view.findViewById(R.id.list_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //Toast.makeText(getActivity(),arrayListCat.get(position).getPostName().toString(),Toast.LENGTH_LONG).show();
                postID = arrayList.get(position).getCat_id();
                name = arrayList.get(position).getCat_name().toString();
                check = true;
                pd.show();
                catURL = preferances.getSiteURL() + Constance.GET_MENU;
              /*  Request2();
                GetContacts getContacts = new GetContacts();
                getContacts.execute();*/

                RequestQueue queue = Volley.newRequestQueue(getActivity());
                final StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            pd.dismiss();
                            // JSONObject jsonResponse = new JSONObject(response);
                            JSONArray jsonArray = new JSONArray(response);
                            Log.e("ERERRER",response);
                            arrayListCat = new ArrayList<>();

                                for (int i=0; i<jsonArray.length() ; i++){

                                    GetMenuBean bean = new GetMenuBean();
                                    bean.setCat_id(jsonArray.getJSONObject(i).getInt("cat_id"));
                                    bean.setCat_name(jsonArray.getJSONObject(i).getString("cat_name"));
                                    Log.e("PostName",bean.getCat_name()+"");
                                    arrayListCat.add(bean);

                                }

                                Intent intent = new Intent(getActivity(),SemesterActivity.class);
                                intent.putExtra("arrayList",arrayListCat);
                                intent.putExtra("title",name);
                                startActivity(intent);


                        } catch (Exception e) {
                            //e.printStackTrace();
                            Log.e("ERROR",e+"");
                        }

                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR",error+"");
                        pd.dismiss();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("cat_id", arrayList.get(position).getCat_id()+"");
                        return params;
                    }
                };

                queue.add(stringRequest);


            }
        });

        Request();

        return view;
    }

    private void Request()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    pd.dismiss();
                   // JSONObject jsonResponse = new JSONObject(response);
                 JSONArray jsonArray = new JSONArray(response);
                    Log.e("ERERRER",response);

                        for (int i=0; i<jsonArray.length() ; i++) {

                            GetMenuBean bean = new GetMenuBean();
                            bean.setCat_id(jsonArray.getJSONObject(i).getInt("cat_id"));
                            bean.setCat_name(jsonArray.getJSONObject(i).getString("cat_name"));
                            Log.e("PostName", bean.getCat_name() + "");
                            arrayList.add(bean);

                        }

                        //Log.e("TYPE", jsonResponse.getString("type") + "");

                        ShowResult(arrayList);
                       /* Intent intent = new Intent(getActivity(),CourseListCategory.class);
                        intent.putExtra("arrayListCat",arrayListCat);
                        intent.putExtra("arrayListFile",arrayListFile);
                        startActivity(intent);*/



                } catch (JSONException e) {
                    //e.printStackTrace();
                    Log.e("ERROR",e+"");
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                //params.put("cat_id", preferances.getCat_id());
                return params;
            }
        };

        queue.add(stringRequest);
    }
    public void ShowResult(ArrayList<GetMenuBean> arrayList)
    {
        YearsAdapter adapter = new YearsAdapter(getActivity(),arrayList);
        listView.setAdapter(adapter);
    }

public class OnFragmentInteractionListener {
    }
}
