package app.com.jumedicine.Bean;

/**
 * Created by abu_a on 30-Nov-17.
 */

public class ListText {

    public String name;
    public String hometown;

    public ListText(String name, String hometown) {
        this.name = name;
        this.hometown = hometown;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }
}
