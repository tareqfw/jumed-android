package app.com.jumedicine.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.YearsAdapter;
import app.com.jumedicine.Bean.CourseFile2;
import app.com.jumedicine.Bean.CourseListBean;
import app.com.jumedicine.Bean.CourseListCatBean2;
import app.com.jumedicine.Bean.GetMenuBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class SemesterActivity extends AppCompatActivity {

    String url,catURL,name;
    ArrayList<GetMenuBean> arrayList;
    CourseListBean bean;
    ListView listView ;
    static int postID = 0;
    Boolean check = false;
    ProgressDialog pd ;
    ArrayList<CourseListCatBean2> arrayListCat;
    ArrayList<CourseFile2> arrayListFile;
    Preferances preferances;
    Activity activity;
    TextView title;



    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.e("Size", arrayListCat.size() + "");
            Log.e("Size", arrayListFile.size() + "");
        }catch (Exception e){

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semester);
        // Inflate the layout for this fragment
       // View view = inflater.inflate(R.layout.fragment_years,container,false);
        activity = SemesterActivity.this;
        pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);
        String title_s = getIntent().getExtras().getString("title");
        title = findViewById(R.id.titleTTV);
        title.setText(title_s);
        //this.pd.show();
        preferances = new Preferances(getApplicationContext());
        /*arrayListCat = new ArrayList<>();
        arrayListFile = new ArrayList<>();*/
        url = preferances.getSiteURL()+ Constance.GET_MENU;

        arrayList = (ArrayList<GetMenuBean>)getIntent().getSerializableExtra("arrayList");

        listView = findViewById(R.id.list_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getActivity(),arrayListCat.get(position).getPostName().toString(),Toast.LENGTH_LONG).show();
                postID = arrayList.get(position).getCat_id();
                name = arrayList.get(position).getCat_name().toString();
                check = true;
                pd.show();
                catURL = preferances.getSiteURL()+Constance.GET_CATEGORY;
              /*  Request2();
                GetContacts getContacts = new GetContacts();
                getContacts.execute();*/


                StringRequest postRequest = new StringRequest(Request.Method.POST, catURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("RESPONSEDDDDD",response );
                                try {
                                   pd.dismiss();
                                    JSONObject jsonResponse = new JSONObject(response);

                                    String type = jsonResponse.getString("type");
                                    if (type.equals("category")){
                                       /* Intent intent = new Intent(getApplicationContext(),CourseListCategory.class);
                                        intent.putExtra("arrayListCat",courseArrayList);
                                        intent.putExtra("arrayListFile",fileArrayList);
                                        startActivity(intent);*/

                                /*    JSONArray jsonPosts ,jsonFile;
                                    jsonPosts = jsonResponse.getJSONArray("posts");
                                    Log.e("obj",jsonPosts+"");
                                    //JSONArray postArray = new JSONArray(jsonResponse.getJSONArray("posts"));
                                    arrayListCat = new ArrayList<>();
                                    arrayListFile = new ArrayList<>();
                                    for (int i=0; i<jsonPosts.length() ; i++){
                                        CourseListCatBean2 bean = new CourseListCatBean2();
                                        bean.setPostID(jsonPosts.getJSONObject(i).getInt("postID"));
                                        bean.setPostName(jsonPosts.getJSONObject(i).getString("postName"));
                                        Log.e("PostName",bean.getPostName()+"");
                                        arrayListCat.add(bean);
                                    }

                                    Log.e("SizeFile",arrayListCat.size()+"");

                                    try {



                                        jsonFile = jsonResponse.getJSONArray("file");
                                        for (int i = 0; i < jsonFile.length(); i++) {
                                            CourseFile2 bean = new CourseFile2();
                                            bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                            bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                            bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                            Log.e("PostName", bean.getName() + "");
                                            arrayListFile.add(bean);
                                        }
                                        Log.e("SizeFile", arrayListFile.size() + "");
                                    }catch (Exception e){
                                        Log.e("ERROR13264",e+"");
                                          /*  Intent intent = new Intent(getActivity(),CourseListCategory.class);
                                            intent.putExtra("arrayListCat",arrayListCat);
                                            intent.putExtra("arrayListFile",arrayListFile);
                                            startActivity(intent);*/
                                        // }
                                        //Log.e("TYPE", jsonResponse.getString("type") + "");*/

                                        Intent intent = new Intent(getApplicationContext(),CourseListCategory2.class);
                                        intent.putExtra("name",name);
                                    /*intent.putExtra("arrayListFile",arrayListFile);*/

                                        intent.putExtra("res",response);
                                        startActivity(intent);


                                    }else{
                                        JSONArray jsonFile;
                                        String title , content;
                                        jsonFile = jsonResponse.getJSONArray("file");

                                        title = jsonResponse.getString("title");
                                        content = jsonResponse.getString("content");
                                        arrayListFile = new ArrayList<>();

                                        for (int i=0; i<jsonFile.length() ; i++){
                                            CourseFile2 bean = new CourseFile2();
                                            bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                            bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                            bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                            Log.e("PostName",bean.getName()+"");
                                            arrayListFile.add(bean);
                                        }

                                        Log.e("title",title);
                                        Log.e("content",content);
                                        Log.e("SizeFile",arrayListFile.size()+"");

                                        Intent intent = new Intent(getApplicationContext(),FileListActivity2.class);
                                        intent.putExtra("title",title);
                                        intent.putExtra("drName",content);
                                        intent.putExtra("name",name);
                                        //intent.putExtra("arrayListFile",arrayListFile);
                                        intent.putExtra("res",response);
                                        startActivity(intent);

                                    }

                                } catch (JSONException e) {
                                    //e.printStackTrace();
                                    Log.e("ERROR11",e+"");
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                Log.e("ERROR231",error+"");
                                pd.dismiss();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("cat_id", postID+"");
                        return params;
                    }
                };
                Volley.newRequestQueue(getApplicationContext()).add(postRequest);

                /*Intent intent = new Intent(getActivity(), CourseListCategory.class);
                intent.putExtra("postID",postID+"");
                getActivity().startActivity(intent);*/
            }
        });
       // Request();

        YearsAdapter adapter = new YearsAdapter(getApplicationContext(),arrayList);
        listView.setAdapter(adapter);

    }

    private void Request()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    pd.dismiss();
                    // JSONObject jsonResponse = new JSONObject(response);
                    JSONArray jsonArray = new JSONArray(response);
                    Log.e("ERERRER",preferances.getCat_id());
                    JSONObject jsonResponse = jsonArray.getJSONObject(0);

                    String type = jsonArray.getJSONObject(0).getString("type");
                    if (type.equals("category")){
                                       /* Intent intent = new Intent(getApplicationContext(),CourseListCategory.class);
                                        intent.putExtra("arrayListCat",courseArrayList);
                                        intent.putExtra("arrayListFile",fileArrayList);
                                        startActivity(intent);*/

                        JSONArray jsonPosts ,jsonFile;
                        //jsonPosts = jsonResponse.getJSONArray("posts");
                        //Log.e("obj",jsonPosts+"");
                        //JSONArray postArray = new JSONArray(jsonResponse.getJSONArray("posts"));
                        arrayList = new ArrayList<>();
                        // arrayListFile = new ArrayList<>();
                        for (int i=0; i<jsonArray.length() ; i++){

                            GetMenuBean bean = new GetMenuBean();
                            bean.setCat_id(jsonArray.getJSONObject(i).getInt("cat_id"));
                            bean.setCat_name(jsonArray.getJSONObject(i).getString("cat_name"));
                            Log.e("PostName",bean.getCat_name()+"");
                            arrayList.add(bean);

                        }

                        // Log.e("SizeFile",arrayListCat.size()+"");

                        try {

                                            /*if (jsonResponse.isNull("file"))
                                                Log.e("IsNull","null");*/

                            jsonFile = jsonResponse.getJSONArray("file");
                            for (int i = 0; i < jsonFile.length(); i++) {
                                CourseFile2 bean = new CourseFile2();
                                bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                Log.e("PostName", bean.getName() + "");
                                arrayListFile.add(bean);
                            }
                            Log.e("SizeFile", arrayListFile.size() + "");
                        }catch (Exception e){
                                          /*  Intent intent = new Intent(getActivity(),CourseListCategory.class);
                                            intent.putExtra("arrayListCat",arrayListCat);
                                            intent.putExtra("arrayListFile",arrayListFile);
                                            startActivity(intent);*/
                        }
                        Log.e("TYPE", jsonResponse.getString("type") + "");

                        ShowResult(arrayList);
                       /* Intent intent = new Intent(getActivity(),CourseListCategory.class);
                        intent.putExtra("arrayListCat",arrayListCat);
                        intent.putExtra("arrayListFile",arrayListFile);
                        startActivity(intent);*/


                    }else{
                        JSONArray jsonFile;
                        String title , content;
                        jsonFile = jsonResponse.getJSONArray("file");

                        title = jsonResponse.getString("title");
                        content = jsonResponse.getString("content");
                        arrayListFile = new ArrayList<>();

                        for (int i=0; i<jsonFile.length() ; i++){
                            CourseFile2 bean = new CourseFile2();
                            bean.setName(jsonFile.getJSONObject(i).getString("name"));
                            bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                            bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                            Log.e("PostName",bean.getName()+"");
                            arrayListFile.add(bean);
                        }

                        Log.e("title",title);
                        Log.e("content",content);
                        Log.e("SizeFile",arrayListFile.size()+"");

                        /*Intent intent = new Intent(getActivity(),FileListActivity.class);
                        intent.putExtra("title",title);
                        intent.putExtra("drName",content);
                        intent.putExtra("arrayListFile",arrayListFile);
                        startActivity(intent);*/

                    }


                } catch (JSONException e) {
                    //e.printStackTrace();
                    Log.e("ERROR",e+"");
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("cat_id", preferances.getCat_id());
                return params;
            }
        };

        queue.add(stringRequest);
    }
    public void ShowResult(ArrayList<GetMenuBean> arrayList)
    {
        YearsAdapter adapter = new YearsAdapter(getApplicationContext(),arrayList);
        listView.setAdapter(adapter);
    }

}
