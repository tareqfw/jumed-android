package app.com.jumedicine.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;

import app.com.jumedicine.R;

public class DialogRename extends DialogFragment {

    private EditText mEditText;
    private Button saveBtn;
    static String path,name;

    public DialogRename() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static DialogRename newInstance(String title,String path,String name) {
        DialogRename frag = new DialogRename();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("path",path);
        args.putString("name",name);
        frag.setArguments(args);


        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rename_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        mEditText = view.findViewById(R.id.txt_your_name);
        saveBtn = view.findViewById(R.id.saveBtn);
        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        path = getArguments().getString("path");
        name = getArguments().getString("name");
        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //File currentFile = new File("/sdcard/currentFile.txt");
                //File newFile = new File("/sdcard/newFile.txt");
                File currentFile = new File(path+name);
                File newFile = new File(path+mEditText.getText().toString()+".pdf");


                if(rename(currentFile, newFile)){
                    //Success
                    Log.i("12", "Success");
                    Fragment currentFragment = getFragmentManager().findFragmentByTag("download");
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.detach(currentFragment);
                    fragmentTransaction.attach(currentFragment);
                    fragmentTransaction.commit();


                } else {
                    //Fail
                    Log.i("`32", "Fail");
                }
                //DownloadedActivity.reCrate();
                dismiss();
            }
        });
    }

    private boolean rename(File from, File to) {
        return from.getParentFile().exists() && from.exists() && from.renameTo(to);
    }

}