package app.com.jumedicine.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.com.jumedicine.Bean.CourseListCatBean;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 06-Dec-17.
 */

public class CourseListCatAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CourseListCatBean> arrayList;

    public CourseListCatAdapter(Context context, ArrayList<CourseListCatBean> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.course_list_row, null);


        TextView txtTitle =  convertView.findViewById(R.id.tv_list);
        txtTitle.setText(arrayList.get(position).getPostName());
        ImageView icon_list = convertView.findViewById(R.id.icon_list);
        Glide.with(context).load(arrayList.get(position).getIcon()).into(icon_list);
        Log.e("ADAPTER",arrayList.get(position).getPostName());


        return convertView;
    }
}
