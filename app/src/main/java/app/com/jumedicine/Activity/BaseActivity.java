package app.com.jumedicine.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import app.com.jumedicine.Adapter.NavDrawerListAdapter;
import app.com.jumedicine.Bean.Bean_Notifications;
import app.com.jumedicine.Bean.NavDrawerItem;
import app.com.jumedicine.DataBase.DataBaseHelper;
import app.com.jumedicine.Notification.Config;
import app.com.jumedicine.Notification.MyFirebaseInstanceIDService;
import app.com.jumedicine.Notification.MyFirebaseMessagingService;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class BaseActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    SlidingMenu menu;
    Fragment mCurruntFragment;
    TextView title;
    ImageView btn;
    ArrayList<NavDrawerItem> navDrawerItems;
    Preferances preferances;
  //  DataBaseHelper db;
    ImageView searchBtn,notiBtn;
    EditText searchEdt;
    String url;
    ProgressDialog pd ;
    ArrayList<Bean_Notifications> arrayList;
    //MyBroadcastReceiver receiver;
    static Context context;
    static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferances = new Preferances(getApplicationContext());

        pd = new ProgressDialog(BaseActivity.this);
        pd.setMessage("Searching..");
        pd.setCancelable(false);
        FirebaseApp.initializeApp(this);
        context =getApplicationContext();
        activity = BaseActivity.this;

      //  db = new DataBaseHelper(getApplicationContext());

        try {
            Intent intent = getIntent();
            String id = intent.getStringExtra("post_id");
            String name = intent.getStringExtra("type");
            Log.e("Id",id);
            Log.e("type",name);
           // db.insertNotifications(name,Integer.parseInt(id));
            Intent intent2 = new Intent(getApplicationContext(),FileListActivityGetPostNoti.class);
            intent2.putExtra("id",Integer.parseInt(id));
            startActivity(intent2);
        }catch (Exception e){
            Log.e("ERROr",e+"");
        }



   /*     IntentFilter intentFilter = new IntentFilter(Constance.BROADCAST);
        receiver = new MyBroadcastReceiver();
        registerReceiver(receiver, intentFilter);*/





    // [START subscribe_topics]
        //FirebaseMessaging.getInstance().subscribeToTopic("Doctor2016");
        // [END subscribe_topics]

        // Log and toast
        String msg = "Subscribe To News";
        Log.d("TAG", msg);
        //Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();

        Log.e("",FirebaseInstanceId.getInstance().getToken()+"");

        url = preferances.getSiteURL()+Constance.SEARCH;
        if (preferances.getSiteURL().equals("")) {
            Intent intent4 = new Intent(getApplicationContext(), FirstActivity.class);
            startActivity(intent4);
            finish();
        }else{

        }
        if (!isNetworkConnected())
        {
            Intent intent2 = new Intent(getApplicationContext(),NoInternetConnectionActivity.class);
            startActivity(intent2);
        }




        try {
        //    arrayList = db.getAllNotiofications();
           // Toast.makeText(getApplicationContext(),arrayList.size()+"",Toast.LENGTH_LONG).show();
         //   Log.e("SIZE", arrayList.size() + "");
            /*Log.e("1", arrayList.get(0).getPostName() + "");
            Log.e("2", arrayList.get(0).getPostName() + "");
            Log.e("3", arrayList.get(0).getPostName() + "");*/

        }catch (Exception e){
            Log.e("ERROR",e+"");
        }


        btn = findViewById(R.id.menuBtn);
        title = findViewById(R.id.titlePage);

        notiBtn = findViewById(R.id.notiBtn);
        notiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });
        searchEdt = findViewById(R.id.searchEdt);

        searchBtn = findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchEdt.getText().length()==0){
                    Toast.makeText(getApplicationContext(),"Search Field is required",Toast.LENGTH_LONG).show();
                }
                else{
                    pd.show();
                    Request();
                }
            }
        });

        searchEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    if (searchEdt.getText().length()==0){
                        Toast.makeText(getApplicationContext(),"Search Field is required",Toast.LENGTH_LONG).show();
                    }
                    else{
                        pd.show();
                        Request();
                    }

                    return true;
                }

                return false;
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.showMenu();
            }
        });

        Fragment fragment = new CourseList();
        String fragClassName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fragmentManager.beginTransaction();
        fTransaction.addToBackStack(fragClassName).replace(R.id.frame_container, fragment, fragClassName).commit();
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        Create_Menu();
        pd.show();
        RequestOne();

    }

    @Override
    public void onBackPressed() {
        if (menu.isMenuShowing()) {
            menu.toggle();
        } else {
            super.onBackPressed();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
            finish();
        }
    }


    private void Create_Menu() {
        menu = new SlidingMenu(BaseActivity.this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.shadow));
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(BaseActivity.this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.custom_menu);
     //   if (Locale.getDefault().getDisplayLanguage().equals("English"))
         //   menu.setMode(SlidingMenu.LEFT);
      //  else
         //   menu.setMode(SlidingMenu.LEFT);

        // load slide menu items
        String[] navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        if (!preferances.getYears().equals(""))
        navMenuTitles[1] = "Years List";

        navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new NavDrawerItem(0, navMenuTitles[0],R.drawable.ic_speaker_notes_black_48dp));
        navDrawerItems.add(new NavDrawerItem(1, navMenuTitles[1],R.drawable.ic_book_black_48dp));
        navDrawerItems.add(new NavDrawerItem(2, navMenuTitles[2],R.drawable.ic_file_download_black_48dp));
        navDrawerItems.add(new NavDrawerItem(3, navMenuTitles[3],R.drawable.ic_backup_black_48dp));
        navDrawerItems.add(new NavDrawerItem(4, navMenuTitles[4],R.drawable.ic_settings_black_48dp));
        navDrawerItems.add(new NavDrawerItem(5, navMenuTitles[5],R.drawable.ic_book_black_48dp));
        navDrawerItems.add(new NavDrawerItem(6, navMenuTitles[6],R.drawable.ic_book_black_48dp));
        navDrawerItems.add(new NavDrawerItem(7, navMenuTitles[7],R.drawable
                .ic_feedback_black_48dp ));
        navDrawerItems.add(new NavDrawerItem(8, navMenuTitles[8],R.drawable.ic_info_black_48dp));


        // Recycle the typed array
        ListView mDrawerList =  findViewById(R.id.list_view);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        NavDrawerListAdapter adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

    }

    public void displayViewMenu(int position, String... keys) {
        // update the main content by replacing fragments
        Fragment fragment = null;
       switch (position) {
            case 0:
                fragment = new CourseList();
                Log.e("1","1");
                //fragment = new MainActivityMenu();
                break;
            case 1:
                fragment = new YearsFragment();
                Log.e("2","2");
               // startActivity(new Intent(getApplicationContext(), ChangePassword.class));
                break;
            case 2:
                Log.e("3","3");
                fragment = new DownloadedActivity();
                title.setText(navDrawerItems.get(position).getTitle());
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    boolean found = false;
                    mCurruntFragment = fragmentManager.findFragmentById(R.id.frame_container);
                    String fragClassName = fragment.getClass().getName();
                    if (mCurruntFragment != null) {
                        String fragClassNameCurrent = mCurruntFragment.getClass().getName();
                        if (fragClassNameCurrent.equals(fragClassName)) {
                            found = true;
                        }
                    }

                    if (found) {
                        getSupportFragmentManager().popBackStack();
                    }

                    mCurruntFragment = fragment;
                    ft.replace(R.id.frame_container, fragment, "download");
                    ft.addToBackStack(fragClassName);
                    ft.commit();

                    return;
           case 3:

                Log.e("4","4");
                Log.e("OneDrive",preferances.getOneDriveLink());
                Uri uri = Uri.parse(preferances.getOneDriveLink());
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
               // browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    this.startActivity(browserIntent);
                }catch (Exception e){
                    Log.e("ERROR",e+"");
                }
              /*  if (Locale.getDefault().getDisplayLanguage().equals("English")) {
                    MenaTracksUtill.saveLocale("ar", this);
                    MenaTracksUtill.changeLang("ar",this);
                    //   restartActivity() ;
                }
                else  {
                    MenaTracksUtill.saveLocale("en", this);
                    MenaTracksUtill.changeLang("en",this);
                    //restartActivity() ;
                }
*/          case 4:

               fragment = new Setting();
               break;
           case 5:
               fragment = new RaedFragment();
               break;
           case 6:
               fragment = new BooksBankFragment();
               break;
           case 7:

               fragment = new FeedBack();
               break;
           case 8:
               fragment = new AboutUs();
               break;

        }

        title.setText(navDrawerItems.get(position).getTitle());
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            boolean found = false;
            mCurruntFragment = fragmentManager.findFragmentById(R.id.frame_container);
            String fragClassName = fragment.getClass().getName();
            if (mCurruntFragment != null) {
                String fragClassNameCurrent = mCurruntFragment.getClass().getName();
                if (fragClassNameCurrent.equals(fragClassName)) {
                    found = true;
                }
            }

            if (found) {
                getSupportFragmentManager().popBackStack();
            }

            mCurruntFragment = fragment;
            ft.replace(R.id.frame_container, fragment, fragClassName);
            ft.addToBackStack(fragClassName);
            ft.commit();
        }
    }

    static void refreshDownloaded(){

    }

    @Override
    public void onBackStackChanged() {

    }


    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            if(position!=3){
                menu.toggle();
                int ServiceID = ((NavDrawerItem) parent.getItemAtPosition(position)).getID();
                displayViewMenu(ServiceID);
           }
            else
            {
                try {
                    Uri webpage = Uri.parse(preferances.getOneDriveLink());
                    Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
                    startActivity(webIntent);
                    menu.toggle();
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Link is not correct",Toast.LENGTH_LONG).show();
                    Log.e("ERROR",e+"");
                }
            }

        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void Request()
    {
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("RESPONSEDDDDD",response );
                        try {

                            pd.dismiss();
                            Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                            intent.putExtra("res",response);
                            intent.putExtra("title",searchEdt.getText().toString());
                            startActivity(intent);

                        } catch (Exception e) {
                            //e.printStackTrace();
                            pd.dismiss();

                            Log.e("ERROR",e+"");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        pd.dismiss();

                        // pd.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("s", searchEdt.getText()+"");
                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(postRequest);
    }

public static void Xxx(){

        activity.recreate();

}
    public static void Toast(){

        Toast.makeText(context,"you have a new notification",Toast.LENGTH_LONG).show();

    }

    private void RequestOne()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, preferances.getSiteURL()+Constance.GET_GENERAL_SETTINGS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObject = new JSONObject(response);




                    Log.e("ONEDRIVE",preferances.getOneDriveLink()+"");
                    //  ShowResult(arrayList);
                    try {
                        ShowMsg(jObject.getString("one_drive"));
                    }catch (Exception e){
                        Log.e("ERROR",e+"");
                        pd.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.dismiss();
                    Log.e("ERROR",e+"");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
            }
        });
        queue.add(stringRequest);
    }



    public  void ShowMsg(String onedrive){
      //  Toast.makeText(getActivity(), "Saved", Toast.LENGTH_LONG).show();
        preferances.setOneDriveLink(onedrive);
        pd.dismiss();
       // BaseActivity.Xxx();
    }
}
