package app.com.jumedicine.Activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.jumedicine.Bean.BooksBankBean;
import app.com.jumedicine.Bean.ReadBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;
import app.com.jumedicine.Utill.VolleyRequest;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BooksBankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class BooksBankFragment extends Fragment {

    static EditText name , email,message,location;
    Button send;
    Preferances preferances;
    static String name_ , email_,message_;
    static ProgressDialog pd ;
    RelativeLayout messageRl;
    VolleyRequest req;
    static String url;
    static Context context;
    ImageView btn_fb;

    private BooksBankFragment.OnFragmentInteractionListener mListener;

    public BooksBankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_books_bank,container,false);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait...");
        preferances = new Preferances(getActivity());

        context = getActivity();
        url = preferances.getSiteURL() + Constance.MAILER;
        Log.e("URL",url);

        messageRl = view.findViewById(R.id.messageRl);
        name = view.findViewById(R.id.nameEdtR);
        email = view.findViewById(R.id.emailEdtR);
        message = view.findViewById(R.id.messageEdtR);
        location = view.findViewById(R.id.locEdtR);

        messageRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message.requestFocus();
            }
        });

        btn_fb = view.findViewById(R.id.btn_fb);

        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook" +
                            ".com/booksbank2018"));
                    startActivity(intent);
                } catch(Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook" +
                            ".com/booksbank2018")));
                }
            }
        });
        send = view.findViewById(R.id.sendR);

        if (!preferances.getName().equals(""))
        {
            name.setText(preferances.getName());
        }

        if (!preferances.getPhone().equals(""))
        {
            email.setText(preferances.getPhone());
        }

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("")) {
                    Toast.makeText(getActivity(),"Please Fill The Name",Toast.LENGTH_LONG).show();
                    name.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(name, InputMethodManager.SHOW_IMPLICIT);
                }else if (email.getText().toString().equals("")){
                    Toast.makeText(getActivity(),"Please Fill The Email",Toast.LENGTH_LONG).show();
                    email.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(email, InputMethodManager.SHOW_IMPLICIT);
                }else if (message.getText().toString().equals("")){
                    Toast.makeText(getActivity(),"Please Fill The Message",Toast.LENGTH_LONG).show();
                    message.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(message, InputMethodManager.SHOW_IMPLICIT);
                }else if (location.getText().toString().equals("")) {
                    Toast.makeText(getActivity(),"Please Fill The Message",Toast.LENGTH_LONG).show();
                    location.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.showSoftInput(message, InputMethodManager.SHOW_IMPLICIT);
                }   else {
                    name_ = name.getText().toString();
                    email_ = email.getText().toString();
                    preferances.setName(name.getText().toString());
                    preferances.setPhone(email.getText().toString());


                    message_ = message.getText().toString();
                    BooksBankBean bean = new BooksBankBean();
                    bean.setEmail(email_);
                    bean.setMassage(message_);
                    bean.setName(name_);
                    bean.setType("");
                    bean.setLocation(location.getText().toString());
                    /*bean.setEmail("mm");
                    bean.setMassage("asas");
                    bean.setName("sasa");
                    bean.setType("raed");*/
                    Gson gson = new Gson();
                    String json = gson.toJson(bean);
                    Log.e("JSON",json);
                    pd.show();
                    String x ="massage:asdasdasdasdasdsad email:mmm name:Mohammad type:raed";
                   // Request(json);
                    DownloadInfoOfWeather d = new DownloadInfoOfWeather();
                    d.execute();
                }
            }
        });

        return view;
    }


    private void Request(String json)
    {

        try {

        req = new VolleyRequest(Request.Method.POST, preferances.getSiteURL() + Constance.MAILER,getHeaders(), json, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.v("Response:%n %s", response);
                Log.e("REs",response);
                try {
                    //if (response.equals("sucess")){
                        name.setText("");
                        email.setText("");
                        message.setText("");
                        Toast.makeText(getActivity(),"successfully send",Toast.LENGTH_LONG).show();
                   // }
                    pd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                error.printStackTrace();
                pd.dismiss();

            }
        });
        Volley.newRequestQueue(getActivity()).add(req);
        }catch (Exception e){
            Log.e("ERROR",e+"");
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,preferances.getSiteURL()+ Constance.MAILER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Response",response);
                if (response.equals("sucess")){


                }
                pd.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("name",name_);
                params.put("email",email_);
                params.put("massage",message_);
                params.put("type","");
                params.put("location",location.getText().toString());

                Log.e("params",params+"");
                return params;
            }
        };

        queue.add(stringRequest);
    }

    public class OnFragmentInteractionListener {
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String>  params = new HashMap<String, String>();


        return params;
    }
    public String getBodyContentType()
    {
        return "";
    }

    public static class DownloadInfoOfWeather extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name", name_));
                nameValuePairs.add(new BasicNameValuePair("email", email_));
                nameValuePairs.add(new BasicNameValuePair("massage", message_));
                nameValuePairs.add(new BasicNameValuePair("type", ""));
                nameValuePairs.add(new BasicNameValuePair("location", location.getText().toString()));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            //name.setText("");
            //email.setText("");
            message.setText("");
            Toast.makeText(context,"successfully send",Toast.LENGTH_LONG).show();


        }
    }


}
