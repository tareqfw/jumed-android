package app.com.jumedicine.Bean;

import java.io.Serializable;

/**
 * Created by abu_a on 08-Dec-17.
 */

public class CourseFile implements Serializable {

    String file,name,group_name,date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
