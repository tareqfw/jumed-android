package app.com.jumedicine.Bean;

/**
 * Created by abu_a on 08-Dec-17.
 */

public class CourseFile2  {

    String file,name,group_name;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
