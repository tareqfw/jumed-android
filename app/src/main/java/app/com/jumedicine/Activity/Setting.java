package app.com.jumedicine.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.NothingSelectedSpinnerAdapter;
import app.com.jumedicine.Adapter.SpinnerAdapter;
import app.com.jumedicine.Bean.SiteBean;
import app.com.jumedicine.DataBase.DataBaseHelper;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class Setting extends Fragment{

    Button btn_changeYears ;
    ArrayList<SiteBean> arrayList;
    View view;
//    FragmentTransaction ft = getFragmentManager().beginTransaction();
    Spinner spinner;
    Button save;
    TextView btnSpi;
    Preferances preferances;
    Boolean firstTime = true;
    int positionID;
    String lastName;
    String st2,st,st3;
    ProgressDialog pd;
    String name;
    Switch aSwitch;
    String url="";

    private Setting.OnFragmentInteractionListener fragmentInteractionListener;

    public Setting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting,container,false);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        save = view.findViewById(R.id.saveBtn);
        aSwitch = view.findViewById(R.id.switchNotification);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (name.equals(preferances.getSiteName())) {

                    } else {
                        //DataBaseHelper db = new DataBaseHelper(getActivity());
                       // db.DeleteAllNotifications();


                        FirebaseMessaging.getInstance().unsubscribeFromTopic(st2);

                        preferances.setSelectedURL(arrayList.get(positionID - 1).getSiteUrl(), arrayList.get(positionID - 1).getSiteName());

                        FirebaseMessaging.getInstance().subscribeToTopic(st);
                        try {
                            pd.show();
                        }catch (Exception e){
                            Log.e("ERROR_PD",e+"");
                        }

                        RequestSettings();
                /*BaseActivity baseActivity = new BaseActivity();
                baseActivity.displayViewMenu(0,"");*/
                    }
                }catch (Exception e){
                    Log.e("ERROR",e+"");
                }
            }
        });
        preferances = new Preferances(getActivity());
        lastName = preferances.getSiteName();
        st2 = lastName.replaceAll("\\s+","");

        if (!preferances.getNotificationOptions())
        {
            aSwitch.setChecked(false);
        }
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                lastName = preferances.getSiteName();
                st3 = lastName.replaceAll("\\s+","");
                if (isChecked){
                    preferances.setNotificationOptions(true);
                    Log.e("1",isChecked+"");
                    FirebaseMessaging.getInstance().subscribeToTopic(st3);
                }else{
                    preferances.setNotificationOptions(false);
                    Log.e("2",isChecked+"");
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(st3);
                }
            }
        });
        spinner = view.findViewById(R.id.spinner);
        btnSpi = view.findViewById(R.id.btnSpi);
        btnSpi.setText(preferances.getSiteName());
        btnSpi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.performClick();
            }
        });
        arrayList = new ArrayList<>();
       // btn_changeYears = view.findViewById(R.id.btn_setting);

        Request();

        return view;
    }

    public class OnFragmentInteractionListener {
    }

    private void Request2()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://doctor2015.jumedicine.com"+Constance.GET_GENERAL_SETTINGS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    SiteBean bean ;
                    JSONObject jObject = new JSONObject(response);
                    JSONArray array = jObject.getJSONArray("sites");
                    for (int i=0 ; i <array.length() ; i++)
                    {
                        bean = new SiteBean();

                        bean.setSiteUrl(array.getJSONObject(i).getString("siteUrl"));
                        bean.setSiteName(array.getJSONObject(i).getString("SiteName"));

                        arrayList.add(bean);
                    }


                 /*   ft.addToBackStack(null);
                    CustomDialogClass cdd=new CustomDialogClass(getActivity(),arrayList);
                    cdd.show(ft,"");*/

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("ERROR",e+"");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
            }
        });
        queue.add(stringRequest);
    }


    private void Request()
    {
        pd.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, Constance.GET_SITE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    SiteBean bean ;
                   // JSONObject jObject = new JSONObject(response);
                    JSONArray array = new JSONArray(response);
                    for (int i=0 ; i <array.length() ; i++)
                    {
                        bean = new SiteBean();

                        bean.setSiteUrl(array.getJSONObject(i).getString("siteUrl"));
                        bean.setSiteName(array.getJSONObject(i).getString("SiteName"));

                        arrayList.add(bean);
                    }

                    ShowResult(arrayList);
                    pd.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.dismiss();
                    Log.e("ERROR",e+"");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        });
        queue.add(stringRequest);
    }

    public void ShowResult(final ArrayList<SiteBean> arrayList)
    {
        SpinnerAdapter adapter = new SpinnerAdapter(
                getActivity(), R.layout.custom_simple,
                arrayList);
        spinner.setAdapter(
                new NothingSelectedSpinnerAdapter(adapter, R.layout.custom_simple,
                        getActivity()));


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (firstTime){

                }else {
                    btnSpi.setText(arrayList.get(position-1).getSiteName());
                    positionID = position;

                    st = arrayList.get(position-1).getSiteName().replaceAll("\\s+","");
                    name = arrayList.get(position-1).getSiteName();
                    url = arrayList.get(position-1).getSiteUrl()+Constance.GET_MENU;

                    Log.e("name",name);
                    Log.e("lastName",preferances.getSiteName());
                    //preferances.setSelectedURL(arrayList.get(position - 1).getSiteUrl(), arrayList.get(position - 1).getSiteName());
                    //String url = arrayList.get(position).getSiteUrl()+Constance.GET_GENERAL_SETTINGS;
                    // Toast.makeText(getApplicationContext(),position+" "+arrayList.get(position-1).getSiteName(),Toast.LENGTH_LONG).show();
                    // preferances.setSelectedURL(arrayList.get(position-1).getSiteUrl().toString(), arrayList.get(position-1).getSiteName().toString());
                    /*Intent intent = new Intent(getActivity(), BaseActivity.class);
                    startActivity(intent);
                    getActivity().finish();*/
                }
                firstTime = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void RequestSettings()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    Log.e("RESPONSE",response);

                    JSONArray ja_data = new JSONArray(response);

                    Log.e("posts",ja_data.length()+"");
                    preferances.setYears(ja_data.getJSONObject(0).getString("cat_name"));
                    preferances.setCat_id(ja_data.getJSONObject(0).getInt("cat_id")+"");

                    Log.e("posts",preferances.getYears());
                    Log.e("posts",preferances.getCat_id()+"");

                   // Toast.makeText(getActivity(), "Saved", Toast.LENGTH_LONG).show();

                    //pd.dismiss();

                    RequestOne();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("ERROR",e+"");
                    pd.dismiss();
                }
                if (!preferances.getYears().equals("")){
                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    //startActivity(intent);
                    //finish();
                  //  pd.dismiss();
                }
                else {
                    RequestSettings();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();

                return params;
            }
        };

        queue.add(stringRequest);
    }


    private void RequestOne()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, preferances.getSiteURL()+Constance.GET_GENERAL_SETTINGS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObject = new JSONObject(response);




                    Log.e("ONEDRIVE",jObject.getString("one_drive"));
                  //  ShowResult(arrayList);
                    try {
                        ShowMsg(jObject.getString("one_drive"));
                    }catch (Exception e){
                        Log.e("ERROR",e+"");
                        pd.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.dismiss();
                    Log.e("ERROR",e+"");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
            }
        });
        queue.add(stringRequest);
    }



    public  void ShowMsg(String onedrive){
        Toast.makeText(getActivity(), "Saved", Toast.LENGTH_LONG).show();
        preferances.setOneDriveLink(onedrive);
        pd.dismiss();
        BaseActivity.Xxx();
    }
}
