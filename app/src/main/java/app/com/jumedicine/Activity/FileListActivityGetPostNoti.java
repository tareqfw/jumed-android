package app.com.jumedicine.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.FileAdapter;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.Bean.CourseListCatBean;
import app.com.jumedicine.DataBase.DataBaseHelper;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class FileListActivityGetPostNoti extends AppCompatActivity {

    TextView titleTv,drName;
    ArrayList<CourseFile> arrayList;
    ListView listView;
    String file,fName;
    String title_string,drName_string;
    ProgressDialog pd;
    String catURL;
    ArrayList<CourseListCatBean> courseArrayList;
    ArrayList<CourseFile> fileArrayList;
    static int postID = 0;
    static RelativeLayout fileTV_Rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);

        titleTv = findViewById(R.id.titleTTV);
        drName = findViewById(R.id.file_tvDrName);
        listView = findViewById(R.id.list_view_file);

        fileTV_Rl = findViewById(R.id.fileTV_Rl);

        pd = new ProgressDialog(FileListActivityGetPostNoti.this);

        pd.setCancelable(false);

        Preferances preferances = new Preferances(getApplicationContext());
        catURL = preferances.getSiteURL()+ Constance.GET_SINGLE_POST;

        postID = getIntent().getExtras().getInt("id");
        Log.e("postId",postID+"");

        arrayList = new ArrayList<>();
        pd.setMessage("Please Wait...");
        pd.show();
        GetFile();

   /*     StringRequest postRequest = new StringRequest(Request.Method.POST,catURL ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("RESPONSEDDDDD", response);
                        try {
                            pd.dismiss();
                            JSONObject jsonResponse = new JSONObject(response);

                            JSONArray jsonPosts, jsonFile;
                            Log.e("obj", response + "");
                            fileArrayList = new ArrayList<>();
                            String title, content;
                            jsonFile = jsonResponse.getJSONArray("file");

                            title = jsonResponse.getString("title");
                            content = jsonResponse.getString("content");
                            fileArrayList = new ArrayList<>();
                            for (int i = 0; i < jsonFile.length(); i++) {
                                CourseFile bean = new CourseFile();
                                bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                Log.e("PostName", bean.getName() + "");
                                fileArrayList.add(bean);
                            }

                            Log.e("title", title);
                            Log.e("content", content);
                            Log.e("SizeFile", fileArrayList.size() + "");

                            arrayList = fileArrayList;

                            FileAdapter adapter = new FileAdapter(getApplicationContext(), arrayList);
                            listView.setAdapter(adapter);


                            titleTv.setText(title);
                            drName.setText(content);

                            DataBaseHelper db = new DataBaseHelper(getApplicationContext());
                            db.insertNotifications(title, postID);
                            //}

                        } catch (JSONException e) {
                            //e.printStackTrace();
                            Log.e("ERROR",e+"");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        pd.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("cat_id", postID+"");
                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(postRequest);*/

       /* title_string = getIntent().getExtras().getString("title");
        drName_string = getIntent().getExtras().getString("drName");
        arrayList = (ArrayList<CourseFile>) getIntent().getSerializableExtra("arrayListFile");*/


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                file = "";
                file = arrayList.get(position).getFile();
                final AlertDialog alertDialog = new AlertDialog.Builder(FileListActivityGetPostNoti.this,R.style.MyDialogTheme).create();
                alertDialog.setTitle(arrayList.get(position).getName());
                alertDialog.setMessage("What to do with this file ?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Open", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(arrayList.get(position).getFile()));
                        startActivity(browserIntent);

                    } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Download", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            Log.e("if ", "if 0 ");

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(FileListActivityGetPostNoti.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                                // Toast.makeText(getApplicationContext(),"IF 1 ",Toast.LENGTH_LONG).show();

                                ActivityCompat.requestPermissions(FileListActivityGetPostNoti.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","if");
                                // Show an explanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(FileListActivityGetPostNoti.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","else");
                                // Toast.makeText(getApplicationContext(),"IF 2 ",Toast.LENGTH_LONG).show();
                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }else{
                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root,getResources().getString(R.string.folderName));
                            if (!myDir.exists())
                                myDir.mkdirs();
                            String mUrl=file;
                            //RequestQueue mRequestQueue;
                            fName = arrayList.get(position).getName()+".pdf";
                            pd.setMessage("Downloading...");
                            pd.show();
                            FileListActivityGetPostNoti.AsyncTask asyncTask = new FileListActivityGetPostNoti.AsyncTask();
                            asyncTask.execute();

                        }




                    }});

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        alertDialog.dismiss();

                    }});
                alertDialog.show();
            }
        });

    }

    public class AsyncTask extends android.os.AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL u = new URL(file);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                FileOutputStream f = new FileOutputStream(new File("/sdcard/"+getResources().getString(R.string.folderName), fName));


                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ( (len1 = in.read(buffer)) > 0 ) {
                    f.write(buffer,0, len1);
                }
                f.close();
            }catch (Exception e){
                Log.e("ERROR",e+"");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            Toast.makeText(getApplicationContext(),"Downloaded",Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

        public void GetFile(){
            StringRequest postRequest = new StringRequest(Request.Method.POST,catURL ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("RESPONSEDDDDD", response);
                            try {
                                pd.dismiss();
                                JSONObject jsonResponse = new JSONObject(response);

                                //String type = jsonResponse.getString("type");
                                // if (type.equals("category")){

                                JSONArray jsonPosts, jsonFile;
                               // jsonPosts = jsonResponse.getJSONArray("posts");
                               Log.e("obj", response + "");
                                //JSONArray postArray = new JSONArray(jsonResponse.getJSONArray("posts"));
                             //   courseArrayList = new ArrayList<>();
                                fileArrayList = new ArrayList<>();
                             /*   for (int i = 0; i < jsonPosts.length(); i++) {
                                    CourseListCatBean bean = new CourseListCatBean();
                                    bean.setPostID(jsonPosts.getJSONObject(i).getInt("postID"));
                                    bean.setPostName(jsonPosts.getJSONObject(i).getString("postName"));
                                    Log.e("PostName", bean.getPostName() + "");
                                    courseArrayList.add(bean);
                                }*/

                                //Log.e("SizeFile", courseArrayList.size() + "");
                               /* jsonFile = jsonResponse.getJSONArray("file");
                                for (int i = 0; i < jsonFile.length(); i++) {
                                    CourseFile bean = new CourseFile();
                                    bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                    bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                    bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                    Log.e("PostName", bean.getName() + "");
                                    fileArrayList.add(bean);
                                }
                                Log.e("SizeFile", fileArrayList.size() + "");

                                Log.e("TYPE", jsonResponse.getString("type") + "");

                                Intent intent = new Intent(getApplicationContext(), CourseListCategory.class);
                                intent.putExtra("arrayListCat", courseArrayList);
                                intent.putExtra("arrayListFile", fileArrayList);
                                startActivity(intent);*/


                           // }else{
                                String title, content;
                                jsonFile = jsonResponse.getJSONArray("file");

                                title = jsonResponse.getString("title");
                                content = jsonResponse.getString("content");
                                fileArrayList = new ArrayList<>();
                                for (int i = 0; i < jsonFile.length(); i++) {
                                    CourseFile bean = new CourseFile();
                                    bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                    bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                    bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                    Log.e("PostName", bean.getName() + "");
                                    fileArrayList.add(bean);
                                }


                                Log.e("title", title);
                                Log.e("content", content);
                                Log.e("SizeFile", fileArrayList.size() + "");

                            /*    Intent intent = new Intent(getApplicationContext(), FileListActivity.class);
                                intent.putExtra("title", title);
                                intent.putExtra("drName", content);
                                intent.putExtra("arrayListFile", fileArrayList);*/
                                // startActivity(intent);

                                arrayList = fileArrayList;

                                if (fileArrayList.size()==0){
                                    fileTV_Rl.setVisibility(View.GONE);
                                }else{
                                    fileTV_Rl.setVisibility(View.VISIBLE);
                                }

                                FileAdapter adapter = new FileAdapter(getApplicationContext(), arrayList);
                                listView.setAdapter(adapter);


                                titleTv.setText(title);
                                drName.setText(content);

                                DataBaseHelper db = new DataBaseHelper(getApplicationContext()) ;
                               // if (!db.CheckAllNotifications(postID)) {
                                    db.insertNotifications(title, postID);
                               // }
                                //}

                            } catch (JSONException e) {
                                //e.printStackTrace();
                                Log.e("ERROR",e+"");
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            pd.dismiss();
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<>();
                    params.put("post_id", postID+"");
                    return params;
                }
            };
            Volley.newRequestQueue(getApplicationContext()).add(postRequest);
        }


}
