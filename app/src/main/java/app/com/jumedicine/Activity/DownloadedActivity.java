package app.com.jumedicine.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import app.com.jumedicine.Adapter.DownloadedAdapter;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.R;

public class DownloadedActivity extends Fragment {

    private DownloadedActivity.OnFragmentInteractionListener mListener;
    TextView noDataTv;
    ListView DownloadedList;
    ArrayList<CourseFile> arrayList;
    ArrayList<CourseFile> arrayList2;
    static Activity activity;

    public DownloadedActivity() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_downloaded,container,false);

        activity = getActivity();
        noDataTv = view.findViewById(R.id.noDataTv);
        DownloadedList = view.findViewById(R.id.downloadedList);


        DownloadedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // File file = new File(arrayList.get(position).getFile());
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                        +"/"+getResources().getString(R.string.folderName), arrayList.get
                        (position).getName());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Intent intent1 = Intent.createChooser(intent, "Open With");
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                try {
                    startActivity(intent1);
                } catch (ActivityNotFoundException e) {
                    Log.e("ERROR",e+"");
                    // Instruct the user to install a PDF reader here, or something
                }
            }
        });
        arrayList = new ArrayList<>();
        arrayList2 = new ArrayList<>();
        //check for permission
        if(ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
            //ask for permission
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }else {

            String path = "/sdcard/" + getResources().getString(R.string.folderName);
            Log.d("Files", "Path: " + path);

            File directory = new File(path);
            File[] files = directory.listFiles();
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {

                CourseFile bean = new CourseFile();
                bean.setName(files[i].getName());
                bean.setFile(files[i].getAbsolutePath());
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                Date lastModDate = new Date(files[i].lastModified());
                bean.setDate(sdf.format(lastModDate));
                arrayList.add(bean);
                Log.d("Files", "FileName:" + files[i].getName());

            }


        try {
            Collections.sort(arrayList,StuNameComparator);
            for (CourseFile str : arrayList) {

                arrayList2.add(str);
            }
          //  arrayList.remove(1);
        }catch (Exception e){
            Log.e("ERROR",e+"");
        }
           // arrayList2 = new ArrayList<>();
            /*for (int i2 = arrayList.size()-1; i2 == 0  ; i2--) {
                Log.e(i2+"",arrayList.get(i2).getName()+"");
                CourseFile f = new CourseFile();
                f.setDate(arrayList.get(i2).getDate());
                f.setFile(arrayList.get(i2).getFile());
                f.setName(arrayList.get(i2).getName());
                f.setGroup_name(arrayList.get(i2).getGroup_name());
                arrayList2.add(f);
            }*/

            if (arrayList.size()>0){
                noDataTv.setVisibility(View.GONE);
                DownloadedList.setVisibility(View.VISIBLE);
                DownloadedAdapter adapter = new DownloadedAdapter(getActivity(),arrayList2,getActivity());
                DownloadedList.setAdapter(adapter);
            }else{
                noDataTv.setVisibility(View.VISIBLE);
                DownloadedList.setVisibility(View.GONE);
            }
        }
        return view;
    }

    public static Comparator<CourseFile> StuNameComparator = new Comparator<CourseFile>() {

        public int compare(CourseFile s1, CourseFile s2) {
            String StudentName1 = s1.getDate();
            String StudentName2 = s2.getDate();

            //ascending order
            //return StudentName1.compareTo(StudentName2);

            //descending order
            return StudentName2.compareTo(StudentName1);
        }};

    public class OnFragmentInteractionListener {
    }

}