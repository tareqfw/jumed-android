package app.com.jumedicine.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import app.com.jumedicine.Bean.Bean_Notifications;
import app.com.jumedicine.Bean.CourseFile2;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 09-Dec-17.
 */

public class FileAdapter3Noti extends BaseAdapter {

    private Context context;
    private ArrayList<Bean_Notifications> arrayList;

    public FileAdapter3Noti(Context context, ArrayList<Bean_Notifications> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.noti_row, null);


        TextView title = convertView.findViewById(R.id.tvTitle_row);
        TextView fileName =  convertView.findViewById(R.id.fileTXT);
        TextView dateTXT = convertView.findViewById(R.id.dateTXT);

        fileName.setText(arrayList.get(position).getPostName());
        dateTXT.setText(arrayList.get(position).getDate());
       // catName.setText(arrayList.get(position).getGroup_name());
       // fileName.setText(arrayList.get(position).getName());
        //title.setText((position+1)+"");



        return convertView;
    }
}