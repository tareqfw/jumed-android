package app.com.jumedicine.Utill;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: fdoyle
 * Date: 7/20/13
 * Time: 10:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class VolleyRequest extends StringRequest {

    private static final String PROTOCOL_CHARSET = "utf-8";
    Map<String, String> headers;
    String payload;

    public VolleyRequest(int method, String url, Map<String, String> headers, String payload, com.android.volley.Response.Listener<String> listener, com.android.volley.Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.headers = headers;
        this.payload = payload;
        setRetryPolicy(new DefaultRetryPolicy(20000,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.i("url", url);

    }

    public static VolleyRequest get(String url, Map<String, String> headers, com.android.volley.Response.Listener<String> listener, com.android.volley.Response.ErrorListener errorListener) {
        VolleyRequest request = new VolleyRequest(Method.GET, url, headers, null, listener, errorListener);
        request.setTag(url);
        return request;
    }

    public static VolleyRequest post(String url, Map<String, String> headers,  String payload,com.android.volley. Response.Listener<String> listener, com.android.volley.Response.ErrorListener errorListener) {
        VolleyRequest request = new VolleyRequest(Method.POST, url, headers,  payload, listener, errorListener);
        request.setTag(url);
        return request;
    }

    public static VolleyRequest put(String url, Map<String, String> headers, String payload, com.android.volley.Response.Listener<String> listener, com.android.volley.Response.ErrorListener errorListener) {
        VolleyRequest request = new VolleyRequest(Method.PUT, url, headers, payload, listener, errorListener);
        request.setTag(url);
        return request;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public byte[] getBody() {
        try {
            if(payload == null) {
                payload = "";
            }
            return payload.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", payload, PROTOCOL_CHARSET);
            return null;
        }

    }

    @Override
    public String getBodyContentType() {
        return "text/html";
    }
}
