package app.com.jumedicine.Bean;

/**
 * Created by abu_a on 01-Dec-17.
 */
public class NavDrawerItem {
    private int ID;
    private String title;
    private int icon;


    public NavDrawerItem(int ID, String title,int icon) {
        this.title = title;
        this.icon = icon;
        this.ID = ID;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return this.icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

}
