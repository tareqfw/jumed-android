package app.com.jumedicine.Activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.CourseListAdapter;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.Bean.CourseListBean;
import app.com.jumedicine.Bean.CourseListCatBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseList extends Fragment {

    private CourseList.OnFragmentInteractionListener fragmentInteractionListener;
    String url,catURL;
    ArrayList<CourseListBean> arrayList;
    CourseListBean bean;
    ListView listView ;
    static int postID = 0;
    Boolean check = false;
    ProgressDialog pd ;
    ArrayList<CourseListCatBean> arrayListCat;
    ArrayList<CourseFile> arrayListFile;
    String name="";

    public CourseList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_course_list,container,false);
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);
        this.pd.show();
        final Preferances preferances = new Preferances(getActivity());
        url = preferances.getSiteURL()+Constance.GET_COURSELIST;
        arrayList = new ArrayList<>();
        listView = view.findViewById(R.id.list_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getActivity(),arrayList.get(position).getPostName().toString(),Toast.LENGTH_LONG).show();

                name = arrayList.get(position).getPostName().toString();
                postID = arrayList.get(position).getPostID();
                check = true;
                pd.show();
                catURL = preferances.getSiteURL()+Constance.GET_CATEGORY;
              /*  Request2();
                GetContacts getContacts = new GetContacts();
                getContacts.execute();*/


                StringRequest postRequest = new StringRequest(Request.Method.POST, catURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("RESPONSEDDDDD",response );
                                try {
                                    pd.dismiss();
                                    JSONObject jsonResponse = new JSONObject(response);

                                    String type = jsonResponse.getString("type");
                                    if (type.equals("category")){
                                       /* Intent intent = new Intent(getApplicationContext(),CourseListCategory.class);
                                        intent.putExtra("arrayListCat",courseArrayList);
                                        intent.putExtra("arrayListFile",fileArrayList);
                                        startActivity(intent);*/

                                        JSONArray jsonPosts ,jsonFile;
                                        jsonPosts = jsonResponse.getJSONArray("posts");
                                        Log.e("obj",jsonPosts+"");
                                        //JSONArray postArray = new JSONArray(jsonResponse.getJSONArray("posts"));
                                        arrayListCat = new ArrayList<>();
                                        arrayListFile = new ArrayList<>();
                                        for (int i=0; i<jsonPosts.length() ; i++){
                                            CourseListCatBean bean = new CourseListCatBean();
                                            bean.setPostID(jsonPosts.getJSONObject(i).getInt("postID"));
                                            bean.setPostName(jsonPosts.getJSONObject(i).getString("postName"));
                                            bean.setIcon(jsonPosts.getJSONObject(i).getString("icon"));
                                            Log.e("PostName",bean.getPostName()+"");
                                            arrayListCat.add(bean);
                                        }

                                        Log.e("SizeFile",arrayListCat.size()+"");

                                        try {

                                            /*if (jsonResponse.isNull("file"))
                                                Log.e("IsNull","null");*/

                                            jsonFile = jsonResponse.getJSONArray("file");
                                            for (int i = 0; i < jsonFile.length(); i++) {
                                                CourseFile bean = new CourseFile();
                                                bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                                bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                                bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                                Log.e("PostName", bean.getName() + "");
                                                arrayListFile.add(bean);
                                            }
                                            Log.e("SizeFile", arrayListFile.size() + "");
                                        }catch (Exception e){
                                          /*  Intent intent = new Intent(getActivity(),CourseListCategory.class);
                                            intent.putExtra("arrayListCat",arrayListCat);
                                            intent.putExtra("arrayListFile",arrayListFile);
                                            startActivity(intent);*/
                                        }
                                            Log.e("TYPE", jsonResponse.getString("type") + "");

                                        Intent intent = new Intent(getActivity(),CourseListCategory.class);
                                        intent.putExtra("arrayListCat",arrayListCat);
                                        intent.putExtra("arrayListFile",arrayListFile);
                                        intent.putExtra("name",name);
                                        startActivity(intent);


                                    }else{
                                        JSONArray jsonFile;
                                        String title , content;
                                        jsonFile = jsonResponse.getJSONArray("file");

                                        title = jsonResponse.getString("title");
                                        content = jsonResponse.getString("content");
                                        arrayListFile = new ArrayList<>();

                                        for (int i=0; i<jsonFile.length() ; i++){
                                            CourseFile bean = new CourseFile();
                                            bean.setName(jsonFile.getJSONObject(i).getString("name"));
                                            bean.setFile(jsonFile.getJSONObject(i).getString("file"));
                                            bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
                                            Log.e("PostName",bean.getName()+"");
                                            arrayListFile.add(bean);
                                        }

                                        Log.e("title",title);
                                        Log.e("content",content);
                                        Log.e("SizeFile",arrayListFile.size()+"");

                                        Intent intent = new Intent(getActivity(),FileListActivity.class);
                                        intent.putExtra("title",title);
                                        intent.putExtra("drName",content);
                                        intent.putExtra("arrayListFile",arrayListFile);
                                        intent.putExtra("name",name);
                                        startActivity(intent);

                                    }

                                } catch (JSONException e) {
                                    //e.printStackTrace();
                                    Log.e("ERROR",e+"");
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                pd.dismiss();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("cat_id", postID+"");
                        return params;
                    }
                };
                Volley.newRequestQueue(getActivity()).add(postRequest);

                /*Intent intent = new Intent(getActivity(), CourseListCategory.class);
                intent.putExtra("postID",postID+"");
                getActivity().startActivity(intent);*/
            }
        });
        Request();
        return view;
    }

    public class OnFragmentInteractionListener {
    }

    private void Request()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    Log.e("RESPONSE",response);

                    JSONObject jsonObj = new JSONObject(response);

                    JSONArray ja_data = jsonObj.getJSONArray("posts");


                        Log.e("posts",true+"");

                        for (int i = 0; i < ja_data.length(); i++) {
                            jsonObj = ja_data.getJSONObject(i);
                            bean = new CourseListBean();

                            bean.setPostID(jsonObj.getInt("postID"));
                            bean.setPostName(jsonObj.getString("postName"));
                            bean.setPostUrl(jsonObj.getString("postUrl"));
                            bean.setIcon(jsonObj.getString("icon"));

                            arrayList.add(bean);
                        }
                        Log.e("ARRAYLIST", arrayList.size() + "");
                        ShowResult(arrayList);

            pd.dismiss();


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("ERROR",e+"");
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                try {
                    pd.dismiss();
                }catch (Exception e){
                    
                }
            }
        });

        queue.add(stringRequest);
    }
    public void ShowResult(ArrayList<CourseListBean> arrayList)
    {
        CourseListAdapter adapter = new CourseListAdapter(getActivity(),arrayList);
        listView.setAdapter(adapter);

    }


  /*  private void Request2()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,catURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("RESPONSEEESEE",response.toString());

                pd.dismiss();
                /*try {
                    Log.e("RESPONSE",response);
                    JSONObject jsonObj = new JSONObject(response);
                    JSONArray ja_data = jsonObj.getJSONArray("posts");
                    Log.e("posts",ja_data.length()+"");
                    for (int i = 0; i < ja_data.length(); i++) {
                        jsonObj = ja_data.getJSONObject(i);
                        bean = new CourseListCatBean();
                        bean.setPostID(jsonObj.getInt("postID"));
                        bean.setPostName(jsonObj.getString("postName"));
                        bean.setPostUrl(jsonObj.getString("postUrl"));
                        //bean.set(jsonObj.getString("icon"));
                        arrayList.add(bean);
                    }
                    Log.e("ARRAYLIST", arrayList.size() + "");
                    ShowResult(arrayList);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("ERROR",e+"");
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
              //  params.put("Content-Type", "application/json");
                params.put("cat_id",9+"");

                return params;
            }
        };

        queue.add(stringRequest);
    }
    public void ShowResult2(ArrayList<CourseListBean> arrayList)
    {
        CourseListAdapter adapter = new CourseListAdapter(getActivity(),arrayList);
        listView.setAdapter(adapter);

    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getActivity(),"Json Data is downloading",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = catURL;
            String jsonStr = sh.makeServiceCall(url);

            Log.e("LOG-E", "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                   // JSONArray contacts = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                 /*   for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String email = c.getString("email");
                        String address = c.getString("address");
                        String gender = c.getString("gender");

                        // Phone node is JSON Object
                        JSONObject phone = c.getJSONObject("phone");
                        String mobile = phone.getString("mobile");
                        String home = phone.getString("home");
                        String office = phone.getString("office");

                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();

                        // adding each child node to HashMap key => value
                        contact.put("cat_id", postID+"");
                        /*contact.put("name", name);
                        contact.put("email", email);
                        contact.put("mobile", mobile);

                        // adding contact to contact list
                        contactList.add(contact);
                   // }
                } catch (final JSONException e) {
                    Log.e("", "Json parsing error: " + e.getMessage());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e("", "Couldn't get json from server.");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }*/

}
