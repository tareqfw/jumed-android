package app.com.jumedicine.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import app.com.jumedicine.Adapter.FileAdapter2;
import app.com.jumedicine.Bean.CourseFile2;
import app.com.jumedicine.R;

public class FileListActivity2 extends AppCompatActivity {

    TextView title,drName;
    ArrayList<CourseFile2> arrayList;
    ListView listView;
    String titleS,drNameS;
    String title_string,drName_string,response;
    String file,fName;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);

       // Toast.makeText(getApplicationContext(),"File2",Toast.LENGTH_LONG).show();
        //title = findViewById(R.id.file_tvTitle);
        drName = findViewById(R.id.file_tvDrName);
        listView = findViewById(R.id.list_view_file);

        ImageView notiBtn = findViewById(R.id.notiBtn);
        notiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });

        pd = new ProgressDialog(FileListActivity2.this);
        pd.setMessage("Downloading...");
        pd.setCancelable(false);
        arrayList = new ArrayList<>();

        title_string = getIntent().getExtras().getString("name");
        drName_string = getIntent().getExtras().getString("drName");
        title = findViewById(R.id.titleTTV);
        drName.setText(drName_string);
        //title_string = getIntent().getExtras().getString("title");
        title.setText(title_string);
        //arrayList = (ArrayList<CourseFile2>) getIntent().getSerializableExtra("arrayListFile");
        response = getIntent().getExtras().getString("res");
        if (!response.equals(""))
        {
        Parse();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                file = "";
                file = arrayList.get(position).getFile();
                final AlertDialog alertDialog = new AlertDialog.Builder(FileListActivity2.this,R.style.MyDialogTheme).create();
                alertDialog.setTitle(arrayList.get(position).getName());
                alertDialog.setMessage("What to do with this file ?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Open", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(arrayList.get(position).getFile()));
                        startActivity(browserIntent);

                    } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Download", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            Log.e("if ", "if 0 ");

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(FileListActivity2.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                                // Toast.makeText(getApplicationContext(),"IF 1 ",Toast.LENGTH_LONG).show();

                                ActivityCompat.requestPermissions(FileListActivity2.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","if");
                                // Show an explanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(FileListActivity2.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","else");
                                // Toast.makeText(getApplicationContext(),"IF 2 ",Toast.LENGTH_LONG).show();
                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }else{
                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root,getResources().getString(R.string.folderName));
                            if (!myDir.exists())
                                myDir.mkdirs();
                            String mUrl=file;
                            //RequestQueue mRequestQueue;
                            fName = arrayList.get(position).getName()+" "+title_string+".pdf";
                            pd.show();
                            FileListActivity2.AsyncTask asyncTask = new FileListActivity2.AsyncTask();
                            asyncTask.execute();

                        }




                    }});

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        alertDialog.dismiss();

                    }});
                alertDialog.show();
            }
        });
     /*   title.setText(title_string);
        drName.setText(drName_string);

        if (arrayList.size()> 0){
            FileAdapter2 adapter = new FileAdapter2(getApplicationContext(),arrayList);
            listView.setAdapter(adapter);
        }*/

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    // Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root,getResources().getString(R.string.folderName));
                    if (!myDir.exists())
                        myDir.mkdirs();
                    else
                        Log.e("Folder","exist");


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public class AsyncTask extends android.os.AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL u = new URL(file);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                FileOutputStream f = new FileOutputStream(new File("/sdcard/"+getResources().getString(R.string.folderName), fName));


                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ( (len1 = in.read(buffer)) > 0 ) {
                    f.write(buffer,0, len1);
                }
                f.close();
            }catch (Exception e){
                Log.e("ERROR",e+"");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            Toast.makeText(getApplicationContext(),"Downloaded",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void Parse (){

        try
        {
            JSONObject jsonResponse = new JSONObject(response);
        JSONArray jsonFile;
        //String title , content;
        jsonFile = jsonResponse.getJSONArray("file");

        titleS = jsonResponse.getString("title");
        drNameS = jsonResponse.getString("content");
        arrayList = new ArrayList<>();

        for (int i=0; i<jsonFile.length() ; i++){
            CourseFile2 bean = new CourseFile2();
            bean.setName(jsonFile.getJSONObject(i).getString("name"));
            bean.setFile(jsonFile.getJSONObject(i).getString("file"));
            bean.setGroup_name(jsonFile.getJSONObject(i).getString("group_name"));
            Log.e("PostName",bean.getName()+"");
            arrayList.add(bean);
        }

        Log.e("title",titleS);
        Log.e("content",drNameS);
        Log.e("SizeFile",arrayList.size()+"");

       // Intent intent = new Intent(getActivity(),FileListActivity2.class);
                                   /* intent.putExtra("title",title);
                                    intent.putExtra("drName",content);*/
        //intent.putExtra("arrayListFile",arrayListFile);
        //intent.putExtra("res",response);
      //  startActivity(intent);
            title.setText(titleS);
            drName.setText(drNameS);

            if (arrayList.size()> 0){
                FileAdapter2 adapter = new FileAdapter2(getApplicationContext(),arrayList);
                listView.setAdapter(adapter);
            }

    }
        catch (JSONException e) {
        //e.printStackTrace();
        Log.e("ERROR11",e+"");
        }

        }
}
