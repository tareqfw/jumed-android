package app.com.jumedicine.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import app.com.jumedicine.Adapter.FileAdapter;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.R;

public class FileListActivity extends AppCompatActivity {

    TextView title,drName;
    ArrayList<CourseFile> arrayList;
    ListView listView;
    String file,fName;
    String title_string,drName_string;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);


        drName = findViewById(R.id.file_tvDrName);
        listView = findViewById(R.id.list_view_file);

        ImageView notiBtn = findViewById(R.id.notiBtn);
        notiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });

        pd = new ProgressDialog(FileListActivity.this);
        pd.setMessage("Downloading...");
        pd.setCancelable(false);

        arrayList = new ArrayList<>();
        title = findViewById(R.id.titleTTV);
        try {
            title_string = getIntent().getExtras().getString("title");
            drName_string = getIntent().getExtras().getString("drName");
            title.setText(title_string);
        }catch (Exception e){

        }

       // drName_string = getIntent().getExtras().getString("drName");
        arrayList = (ArrayList<CourseFile>) getIntent().getSerializableExtra("arrayListFile");


        drName.setText(drName_string);

        if (arrayList.size()> 0){
            FileAdapter adapter = new FileAdapter(getApplicationContext(),arrayList);
            listView.setAdapter(adapter);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                file = "";
                file = arrayList.get(position).getFile();
                final AlertDialog alertDialog = new AlertDialog.Builder(FileListActivity.this,R.style.MyDialogTheme).create();
                alertDialog.setTitle(arrayList.get(position).getName());
                alertDialog.setMessage("What to do with this file ?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Open", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(arrayList.get(position).getFile()));
                        startActivity(browserIntent);

                    } });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Download", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            Log.e("if ", "if 0 ");

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(FileListActivity.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                                // Toast.makeText(getApplicationContext(),"IF 1 ",Toast.LENGTH_LONG).show();

                                ActivityCompat.requestPermissions(FileListActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","if");
                                // Show an explanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(FileListActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                                Log.e("HERE","else");
                                // Toast.makeText(getApplicationContext(),"IF 2 ",Toast.LENGTH_LONG).show();
                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }else{
                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root,getResources().getString(R.string.folderName));
                            if (!myDir.exists())
                                myDir.mkdirs();
                            String mUrl=file;
                            //RequestQueue mRequestQueue;
                            fName = arrayList.get(position).getName()+" "+title_string+".pdf";
                            pd.show();
                            FileListActivity.AsyncTask asyncTask = new FileListActivity.AsyncTask();
                            asyncTask.execute();

                        }




                    }});

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        alertDialog.dismiss();

                    }});
                alertDialog.show();
            }
        });

    }

    public class AsyncTask extends android.os.AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL u = new URL(file);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                FileOutputStream f = new FileOutputStream(new File("/sdcard/"+getResources().getString(R.string.folderName), fName));


                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ( (len1 = in.read(buffer)) > 0 ) {
                    f.write(buffer,0, len1);
                }
                f.close();
            }catch (Exception e){
                Log.e("ERROR",e+"");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            Toast.makeText(getApplicationContext(),"Downloaded",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
