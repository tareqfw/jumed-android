package app.com.jumedicine.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import app.com.jumedicine.Adapter.DownloadedAdapter;
import app.com.jumedicine.Bean.CourseFile;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 09-Dec-17.
 */

public class DownloadedFileActivity extends Activity {

    TextView noDataTv;
    ListView DownloadedList;
    ArrayList<CourseFile> arrayList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.downloaded_activity_file);

        noDataTv = findViewById(R.id.noDataTv);
        DownloadedList = findViewById(R.id.downloadedList);

        DownloadedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // File file = new File(arrayList.get(position).getFile());
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+getResources().getString(R.string.folderName), arrayList.get(position).getName());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                Intent intent1 = Intent.createChooser(intent, "Open With");
                try {
                    startActivity(intent1);
                } catch (ActivityNotFoundException e) {
                    Log.e("ERROR",e+"");
                    // Instruct the user to install a PDF reader here, or something
                }
            }
        });
        arrayList = new ArrayList<>();
        //check for permission

        if(ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
            //ask for permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }else {

            String path = "/sdcard/" + getResources().getString(R.string.folderName);
            Log.d("Files", "Path: " + path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {
                CourseFile bean = new CourseFile();
                bean.setName(files[i].getName());
                bean.setFile(files[i].getAbsolutePath());
                Date lastModDate = new Date(directory.lastModified());
                bean.setDate(lastModDate.toString());
                arrayList.add(bean);
                Log.d("Files", "FileName:" + files[i].getName());
            }
            if (arrayList.size()>0){
                noDataTv.setVisibility(View.GONE);
                DownloadedList.setVisibility(View.VISIBLE);
                DownloadedAdapter adapter = new DownloadedAdapter(getApplicationContext(),arrayList,DownloadedFileActivity.this);
                DownloadedList.setAdapter(adapter);
            }else{
                noDataTv.setVisibility(View.VISIBLE);
                DownloadedList.setVisibility(View.GONE);
            }
        }

    }
}
