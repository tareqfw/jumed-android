package app.com.jumedicine.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.com.jumedicine.Bean.CourseListCatBean2;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 06-Dec-17.
 */

public class CourseListCatAdapter2 extends BaseAdapter {

    private Context context;
    private ArrayList<CourseListCatBean2> arrayList;

    public CourseListCatAdapter2(Context context, ArrayList<CourseListCatBean2> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.course_list_row, null);


        TextView txtTitle =  convertView.findViewById(R.id.tv_list);
        ImageView imageView = convertView.findViewById(R.id.icon_list);
        Glide.with(context).load(arrayList.get(position).getIcon()).into(imageView);

        txtTitle.setText(arrayList.get(position).getPostName());
        Log.e("ADAPTER",arrayList.get(position).getPostName());

       /* try {
            String x = firstTwo(arrayList.get(position).getIcon().toLowerCase().toString());

            if (x.equals("an")) {
                imageView.setImageResource(R.drawable.anatomy);
            } else if (x.equals("be")) {
                imageView.setImageResource(R.drawable.behavioural);
            } else if (x.equals("bi")) {
                String y = firstFive(arrayList.get(position).getIcon().toString().toLowerCase());
                if (y.equals("biola")) {
                    imageView.setImageResource(R.drawable.biolab);
                } else if (y.equals("biolo")) {
                    imageView.setImageResource(R.drawable.biology);
                } else if (y.equals("biost")) {
                    imageView.setImageResource(R.drawable.biostat);
                }

            } else if (x.equals("ch")) {
                imageView.setImageResource(R.drawable.chemistry);
            } else if (x.equals("cn")) {
                imageView.setImageResource(R.drawable.cns);
            } else if (x.equals("co")) {
                imageView.setImageResource(R.drawable.community);
            } else if (x.equals("cs")) {
                imageView.setImageResource(R.drawable.cs);
            } else if (x.equals("en")) {
                imageView.setImageResource(R.drawable.endocrine);
            } else if (x.equals("ga")) {
                imageView.setImageResource(R.drawable.gardiology);
            } else if (x.equals("ge")) {
                imageView.setImageResource(R.drawable.genetics);
            } else if (x.equals("gl")) {
                imageView.setImageResource(R.drawable.gl);
            } else if (x.equals("gy")) {
                imageView.setImageResource(R.drawable.gyne);
            } else if (x.equals("he")) {
                imageView.setImageResource(R.drawable.hemetology);
            } else if (x.equals("hi")) {
                imageView.setImageResource(R.drawable.histology);
            } else if (x.equals("im")) {
                imageView.setImageResource(R.drawable.immunology);
            } else if (x.equals("in")) {
                imageView.setImageResource(R.drawable.introductory);
            } else if (x.equals("me")) {
                imageView.setImageResource(R.drawable.medicine);
            } else if (x.equals("mi")) {
                imageView.setImageResource(R.drawable.microbiology);
            } else if (x.equals("ms")) {
                imageView.setImageResource(R.drawable.mss);
            } else if (x.equals("or")) {
                imageView.setImageResource(R.drawable.organic);
            } else if (x.equals("pa")) {
                imageView.setImageResource(R.drawable.pathology);
            } else if (x.equals("pe")) {
                imageView.setImageResource(R.drawable.pediatrics);
            } else if (x.equals("ph")) {
                imageView.setImageResource(R.drawable.pharmacology);
            } else if (x.equals("re")) {
                imageView.setImageResource(R.drawable.re_design_respiratory);
            } else if (x.equals("su")) {
                imageView.setImageResource(R.drawable.surgery);
            } else if (x.equals("ur")) {
                imageView.setImageResource(R.drawable.urogenital);
            } else {
                imageView.setImageResource(R.drawable.bookicon_two);
            }
        }catch (Exception e){
            Log.e("ERROR",e+"");
        }*/

        return convertView;
    }


    public String firstTwo(String str) {
        return str.length() < 2 ? str : str.substring(0, 2);
    }
    public String firstFive(String str) {
        return str.length() < 5 ? str : str.substring(0, 5);
    }
}
