package app.com.jumedicine.Utill;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by abu_a on 02-Dec-17.
 */

public class Preferances {

    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sp ;
    Context context;
    public static final String siteURL = "selectedUrl";
    public static final String siteName = "siteName";
    public static final String oneDriveLink = "oneDriveLink";
    public static final String years = "years";
    public static final String phone = "phone";
    public static final String name = "name";
    public static final String notificationOptions = "notificationOptions";
    public static final String cat_id = "cat_id";

    public Preferances(Context context) {
        this.context = context;
        sp = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    public void setSelectedURL (String url,String name)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(siteURL);
        editor.remove(siteName);
        editor.putString(siteURL,url);
        editor.putString(siteName,name);
        editor.commit();
    }


    public void setName (String yearsName)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(name);
        editor.putString(name,yearsName);
        editor.commit();
    }

    public String getName()
    {
        String yearsName;
        yearsName = sp.getString(name,"");
        return yearsName;
    }

    public void setPhone (String yearsName)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(phone);
        editor.putString(phone,yearsName);
        editor.commit();
    }

    public String getPhone()
    {
        String yearsName;
        yearsName = sp.getString(phone,"");
        return yearsName;
    }


    public String getSiteURL ()
    {
        String url;
        url = sp.getString(siteURL,"");
        return url;
    }

    public String getSiteName ()
    {
        String name;
        name = sp.getString(siteName,"");
        return name;
    }


    public void setOneDriveLink (String url)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(oneDriveLink);
        editor.putString(oneDriveLink,url);
        editor.apply();
    }

    public String getOneDriveLink ()
    {
        String link;
        link = sp.getString(oneDriveLink,"");
        return link;
    }


    public void setYears (String yearsName)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(years);
        editor.putString(years,yearsName);
        editor.commit();
    }

    public String getYears()
    {
        String yearsName;
        yearsName = sp.getString(years,"");
        return yearsName;
    }

    public void setNotificationOptions (Boolean options)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(notificationOptions);
        editor.putBoolean(notificationOptions,options);
        editor.commit();
    }

    public Boolean getNotificationOptions()
    {
        Boolean options;
        options = sp.getBoolean(notificationOptions,true);
        return options;
    }

    public void setCat_id (String Cat_id)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(cat_id);
        editor.putString(cat_id,Cat_id);
        editor.commit();
    }

    public String getCat_id()
    {
        String Cat_id;
        Cat_id = sp.getString(cat_id,"");
        return Cat_id;
    }
}
