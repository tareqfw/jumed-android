package app.com.jumedicine.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import app.com.jumedicine.Adapter.FileAdapter3Noti;
import app.com.jumedicine.Adapter.NothingSelectedSpinnerAdapter;
import app.com.jumedicine.Adapter.SpinnerAdapter;
import app.com.jumedicine.Bean.Bean_Notifications;
import app.com.jumedicine.Bean.SiteBean;
import app.com.jumedicine.DataBase.DataBaseHelper;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class NotificationActivity extends AppCompatActivity {

    DataBaseHelper db;
    static ArrayList<Bean_Notifications> arrayList;
    static ListView listView;
    static FileAdapter3Noti adapter3Noti;
    TextView titleTTV,noNotiTv;
    static Preferances preferances;
    static ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        db = new DataBaseHelper(getApplicationContext());
        titleTTV = findViewById(R.id.titleTTV);
        titleTTV.setText("Notification");
        titleTTV.setTextSize(20);

        pd = new ProgressDialog(NotificationActivity.this);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);

        preferances = new Preferances(getApplicationContext());


        noNotiTv = findViewById(R.id.noNotiTv);

            //arrayList = db.getAllNotiofications();


        listView = findViewById(R.id.notiList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent2 = new Intent(getApplicationContext(), FileListActivityGetPostNoti.class);
                intent2.putExtra("id", arrayList.get(position).getPostID());
                startActivity(intent2);
            }
        });

        pd.show();
        Request();


    }


    private void Request()
    {
        RequestQueue queue = Volley.newRequestQueue(NotificationActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,  preferances.getSiteURL()+Constance.GET_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    Log.e("RESPONSE",response);
                    //JSONObject jObject = new JSONObject(response);
                    JSONArray array = new JSONArray(response);
                    arrayList = new ArrayList<>();
                    for (int i=0 ; i <array.length() ; i++)
                    {
                        Bean_Notifications bean = new Bean_Notifications();

                        bean.setPostName(array.getJSONObject(i).getString("postName"));
                        bean.setPostID(array.getJSONObject(i).getInt("postID"));
                        bean.setDate(array.getJSONObject(i).getString("date"));

                        arrayList.add(bean);
                    }

                    ShowResult(arrayList);

                    pd.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.dismiss();
                    Log.e("ERROR",e+"");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
            }
        });
        queue.add(stringRequest);
    }

    public void ShowResult(ArrayList<Bean_Notifications> arrayList) {
        try {
            adapter3Noti = new FileAdapter3Noti(getApplicationContext(), arrayList);
            if (arrayList.size() > 0) {
                listView.setAdapter(adapter3Noti);
            } else {
                noNotiTv.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Log.e("ERROR", e + "");
        }

    }
}
