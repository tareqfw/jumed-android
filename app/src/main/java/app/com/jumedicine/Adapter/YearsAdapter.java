package app.com.jumedicine.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import app.com.jumedicine.Bean.GetMenuBean;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 12-Dec-17.
 */

public class YearsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<GetMenuBean> arrayList;

    public YearsAdapter(Context context, ArrayList<GetMenuBean> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.course_list_row, null);

        ImageView imageView = convertView.findViewById(R.id.icon_list);
        TextView txtTitle = convertView.findViewById(R.id.tv_list);
        txtTitle.setText(arrayList.get(position).getCat_name());

        imageView.setImageResource(R.drawable.bookicon_two);
        return convertView;
    }
}
