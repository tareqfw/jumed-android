package app.com.jumedicine.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import app.com.jumedicine.Bean.SiteBean;
import app.com.jumedicine.R;

/**
 * Created by abu_a on 24-Nov-17.
 */

public class SpinnerAdapter extends ArrayAdapter<SiteBean> {

    Context context;
    int resource;
    ArrayList<SiteBean> value;
    TextView tv;

    public SpinnerAdapter(Context context,
                          int resource, ArrayList<SiteBean> value) {
        super(context, resource, R.id.labelsp, value);
        this.context = context;
        this.resource = resource;
        this.value = value;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(resource, null);
        TextView statues =  layout.findViewById(R.id.labelsp);
        statues.setText(value.get(position).toString());

        return layout;
    }


}
