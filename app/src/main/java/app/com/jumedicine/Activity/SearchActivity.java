package app.com.jumedicine.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.SearchAdapter;
import app.com.jumedicine.Bean.SearchBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class SearchActivity extends AppCompatActivity {

    String response ,url;
    ArrayList<SearchBean> arrayList;
    ListView listView;
    ProgressDialog progressDialog;
    Preferances preferances;
    TextView title;
    ProgressDialog pd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        response = getIntent().getExtras().getString("res");
        arrayList = new ArrayList<>();
        listView = findViewById(R.id.list_view_cat);
        title = findViewById(R.id.titleTTV);
        pd = new ProgressDialog(SearchActivity.this);
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);

        try {
            title.setText(getIntent().getExtras().getString("title"));
        }catch (Exception e){
            Log.e("ERROr",e+"");
        }
        progressDialog = new ProgressDialog(SearchActivity.this);
        progressDialog.setMessage("please Wait...");
        progressDialog.setCancelable(false);

        preferances = new Preferances(getApplicationContext());

        Parse();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

//                if (arrayList.get(position).getPost_type().equals("post"))
          //      {
                    pd.show();
                    url = preferances.getSiteURL()+ Constance.GET_CATEGORY;

                        StringRequest postRequest = new StringRequest(com.android.volley.Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("RESPONSEDDDDD",response );
                                        try {

                                            pd.dismiss();
                                            Intent intent = new Intent(getApplicationContext(),FileListActivity3.class);
                                            intent.putExtra("res",response);
                                            intent.putExtra("title",arrayList.get(position).getPostName());
                                            startActivity(intent);

                                        } catch (Exception e) {
                                            //e.printStackTrace();
                                            Log.e("ERROR",e+"");
                                            pd.dismiss();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();
                                         pd.dismiss();

                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams()
                            {
                                Map<String, String>  params = new HashMap<>();
//                                params.put("post_id", arrayList.get(position).getPostID()+"");
                                params.put("cat_id", arrayList.get(position).getPostID()+"");
                                return params;
                            }
                        };
                        Volley.newRequestQueue(getApplicationContext()).add(postRequest);

              /*  }else if (arrayList.get(position).getPost_type().equals("category")){
                    url = preferances.getSiteURL()+ Constance.GET_CATEGORY;

                }*/

            }
        });

    }

    public void Parse(){

        try {

            Log.e("RESPONSE",response);

            JSONObject obj= new JSONObject(response);
            JSONArray jsonArray = obj.getJSONArray("posts");
            JSONObject jsonObj ;
            //JSONObject jsonObject = jsonObj.getJSONObject("posts");
            //JSONArray ja_data = jsonObject.getJSONArray("data");


            Log.e("posts",true+"");

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObj = jsonArray.getJSONObject(i);
                SearchBean bean = new SearchBean();

                bean.setPostID(jsonObj.getInt("postID"));
                bean.setPostName(jsonObj.getString("postName"));
                bean.setIcon(jsonObj.getString("icon"));
                //bean.setPost_type(jsonObj.getString("post_type"));
                //bean.setIcon(jsonObj.getString("icon"));

                Log.e("1",bean.getPostName());

                arrayList.add(bean);
            }
            Log.e("ARRAYLIST", arrayList.size() + "");
            //ShowResult(arrayList);
            if (arrayList.size()==0){
                Toast.makeText(getApplicationContext(),"There is no result",Toast.LENGTH_LONG).show();
                finish();
            }

            SearchAdapter adapter = new SearchAdapter(getApplicationContext(),arrayList);
            listView.setAdapter(adapter);
            //pd.dismiss();


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERROR",e+"");
        }

    }
}
