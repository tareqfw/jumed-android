package app.com.jumedicine.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.com.jumedicine.Adapter.FileAdapter;
import app.com.jumedicine.Adapter.NothingSelectedSpinnerAdapter;
import app.com.jumedicine.Adapter.SpinnerAdapter;
import app.com.jumedicine.Bean.SiteBean;
import app.com.jumedicine.R;
import app.com.jumedicine.Utill.Constance;
import app.com.jumedicine.Utill.Preferances;

public class FirstActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ArrayList<SiteBean> arrayList;
    Spinner spinner;
    Preferances preferances ;
    Boolean firstTime = true;
    String url="";
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_first);

        preferances = new Preferances(getApplicationContext());

        pd = new ProgressDialog(FirstActivity.this);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        FirebaseApp.initializeApp(this);





        arrayList = new ArrayList<>();
        spinner = findViewById(R.id.spinner);

        String folder_main = getResources().getString(R.string.folderName);

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
        }

        pd.show();
        Request();
    }

    private void Request()
    {
        RequestQueue queue = Volley.newRequestQueue(FirstActivity.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://doctor2015.jumedicine.com"+Constance.GET_GENERAL_SETTINGS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    SiteBean bean ;
                    JSONObject jObject = new JSONObject(response);
                    JSONArray array = jObject.getJSONArray("sites");
                    arrayList = new ArrayList<>();
                    for (int i=0 ; i <array.length() ; i++)
                    {
                        bean = new SiteBean();

                            bean.setSiteUrl(array.getJSONObject(i).getString("siteUrl"));
                            bean.setSiteName(array.getJSONObject(i).getString("SiteName"));

                        arrayList.add(bean);
                    }

                    preferances.setOneDriveLink(jObject.getString("one_drive"));

                    Log.e("ONEDRIVE",preferances.getOneDriveLink()+"");
                        ShowResult(arrayList);

                        pd.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    pd.dismiss();
                    Log.e("ERROR",e+"");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
            }
        });
        queue.add(stringRequest);
    }

    public void ShowResult(ArrayList<SiteBean> arrayList)
    {
        SpinnerAdapter adapter = new SpinnerAdapter(
                this, R.layout.custom_simple,
                arrayList);
        spinner.setAdapter(
                new NothingSelectedSpinnerAdapter(adapter, R.layout.custom_simple,
                        this));

        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

      if (firstTime){

      }else {
          Log.e("postition",arrayList.get(position-1).getSiteUrl()+"");
            url = arrayList.get(position-1).getSiteUrl()+Constance.GET_MENU;
            //Toast.makeText(getApplicationContext(),position-1+" "+arrayList.get(position).getSiteName(),Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(),position-1+" "+arrayList.get(position).getSiteUrl(),Toast.LENGTH_LONG).show();
          Log.e("url",arrayList.get(position-1).getSiteUrl().toString());
          Log.e("name",arrayList.get(position-1).getSiteName().toString());
          String name = arrayList.get(position-1).getSiteName();
          String st = name.replaceAll("\\s+","");
            preferances.setSelectedURL(arrayList.get(position-1).getSiteUrl().toString(), st);
          // [START subscribe_topics]
          FirebaseMessaging.getInstance().subscribeToTopic(st);
          pd.show();
            RequestSettings();
        }
        firstTime = false;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void RequestSettings()
    {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    Log.e("RESPONSE",response);

                    JSONArray ja_data = new JSONArray(response);


                    Log.e("posts",ja_data.length()+"");
                    preferances.setYears(ja_data.getJSONObject(0).getString("cat_name"));
                    preferances.setCat_id(ja_data.getJSONObject(0).getInt("cat_id")+"");

                    Log.e("posts",preferances.getYears());
                    Log.e("posts",preferances.getCat_id()+"");

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("ERROR",e+"");
                }
                if (!preferances.getYears().equals("")){
                    Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
                    startActivity(intent);
                    finish();
                    pd.dismiss();
                }
                else {
                    RequestSettings();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR",error+"");
                pd.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();

                return params;
            }
        };

        queue.add(stringRequest);
    }
}
