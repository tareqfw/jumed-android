package app.com.jumedicine.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import app.com.jumedicine.Bean.Bean_Notifications;

/**
 * Created by abu_a on 02-Dec-17.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Notification.db";
    public static final String TABLE_NAME = "Notifications";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PSOT_NAME= "postName";
    public static final String COLUMN_POST_ID = "postID";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table Notifications " +
                        "(id integer primary key, postName text,postID int)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertNotifications (String p_name, int p_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PSOT_NAME, p_name);
        contentValues.put(COLUMN_POST_ID, p_id);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean DeleteAllNotifications () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL( "delete from "+TABLE_NAME );

        return true;
    }


    public ArrayList<Bean_Notifications> getAllNotiofications() {
        ArrayList<Bean_Notifications> array_list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Notifications", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            Bean_Notifications b = new Bean_Notifications();
            b.setId(res.getInt(res.getColumnIndex(COLUMN_ID)));
            b.setPostID(res.getInt(res.getColumnIndex(COLUMN_POST_ID)));
            b.setPostName(res.getString(res.getColumnIndex(COLUMN_PSOT_NAME)));
            array_list.add(b);
            res.moveToNext();
        }
        return array_list;
    }


}
